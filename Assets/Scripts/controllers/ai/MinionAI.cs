﻿using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEngine;

using UnityEngine.Networking;
using UnityEngine.AI;

using core;
using core.utility;
using core.collection;
using core.forger;
using core.lane;
using controllers;
using managers;

namespace controllers.ai {
    public class MinionAI : UnitAI<Minion> {
        public const short MINION_TETHER_DISTANCE = 400;
        public const short MINION_RETARGET_COOLDOWN = 4;

        private float targetCooldown = 0f;

        float nextRetarget = 6f;
        float nextReposition = 6f;

        // TODO change this. 
        public List<Vector3> laneLine = new List<Vector3>();

        [SerializeField]
        Queue<Vector3> waypoints = new Queue<Vector3>();

        public void ClearBrain () {
            waypoints.Clear();
            nextTarget.Clear();
            waitList.Clear();
            CancelAttackTarget();
        }

        void OnDisable () {
            ClearBrain();
        }

        void OnEnable () {

        }

        public void AddWaypoint (Vector3 point) {
            waypoints.Enqueue(point);
        }

        public void AddWaypoints (IEnumerable<Vector3> points) {
            foreach (var point in points) {
                waypoints.Enqueue(point);
            }
        }

        protected override void DoBrain ()
        {
            base.DoBrain();


            //if (steerAgent != null && PathingManager.Instance.TryGetNearestNode(me.Team, me.Position, out steerAgent.nearestNode))
            //{

            //}

            // FIXME do we not actually need this?

            /*
            if (waypoints.Count == 0) {
                foreach (var victor in laneLine) {
                    waypoints.Enqueue(victor);
                }


            }
            */

            /*
            if (waypoints.Count == 0) {
                var rankThisDist = new MinHeap<float, int>();
                // find the nearest line in lane and go to the next one. 
                for (int i = 0; i < laneLine.Count - 1; i++) {
                    float distanceFromLane = Utils.DistanceFromPointToFiniteLine(laneLine [i], laneLine [i + 1], transform.position).magnitude;
                    rankThisDist.Add(distanceFromLane, i);
                }

//                waypoints.Enqueue(laneLine [rankThisDist.Top()]);



                AttackMove(laneLine [rankThisDist.Top()+1]);

            }
            */

            // Minion reconsiders its targets 

            if (HasTargetUnit && nextRetarget <= GameTime.time) {
                nextRetarget = GameTime.time + MINION_RETARGET_COOLDOWN;
                Retarget();
            }


            /* Trying without this
            if (!InLane && HasTargetUnit) {
                nextRetarget -= Time.deltaTime;
            }
            */

            if (!InLane /*&& !HasTargetUnit && nextReposition <= Time.time */) {
                nextReposition = GameTime.time + RECONSIDER_FREQUENCY;
//                nextRetarget = Time.time + MINION_RETARGET_COOLDOWN;
                // Go back to lane, you're drunk
                var wayarray = waypoints.ToArray();
                var rankThisDist = new MinHeap<float, Vector3>();
                // find the nearest point on the nearest line (between waypoints)
                for (int i = 0; i < laneLine.Count - 1; i++) {
                    float distanceFromLane = Utils.DistanceFromPointToFiniteLine(laneLine [i], laneLine [i + 1], transform.position).magnitude;
                    var nearestPoint = Utils.NearestPointOnFiniteLine(laneLine [i], laneLine [i + 1], transform.position);
                    rankThisDist.Add(distanceFromLane, nearestPoint);

                }

                #if DEBUG_AI
                Debug.LogFormat("distance: {1}, point = {0}", rankThisDist.Top(), rankThisDist.TopRank());
                #endif

//                waypoints.Dequeue();    // Ditch the last one.
//                waypoints.Clear();
                JustMove(rankThisDist.Top());

                // Just stop caring about enemies. 
                nextTarget.Clear();
                // Really just move back to lane.
//                JustMove(rankThisDist.Top());

            }

        }

        protected override void DoIdle () {
            // Minion AttackMoves to the next waypoint
            //if (steerAgent != null)
            //{
            //    steerAgent.moving = true;
            //}

            var rankThisDist = new MinHeap<float, int>();
            // find the nearest line in lane and go to the next one. 
            for (int i = 0; i < laneLine.Count - 1; i++) {
                float distanceFromLane = Utils.DistanceFromPointToFiniteLine(laneLine [i], laneLine [i + 1], transform.position).magnitude;
                rankThisDist.Add(distanceFromLane, i);
            }

                            waypoints.Enqueue(laneLine [rankThisDist.Top()]);


            if ((rankThisDist.Top() + 2 < laneLine.Count)
                && (CLOSE_ENOUGH_RANGE >= Vector3.Distance(laneLine [rankThisDist.Top()], transform.position))) {
                AttackMove(laneLine [rankThisDist.Top() + 2]);
            } else if ((rankThisDist.Top() < laneLine.Count + 1)) {
                AttackMove(laneLine [rankThisDist.Top() + 1]);
            } else {
                AttackMove(laneLine [rankThisDist.Top()]);
            }



//            if (waypoints.Count > 0) {
//                AttackMove(waypoints.Dequeue());
//            } 
//            else {
//                var rankThisDist = new MinHeap<float, int>();
//                // find the nearest line in lane and go to the next one. 
//                for (int i = 0; i < laneLine.Count - 1; i++) {
//                    float distanceFromLane = Utils.DistanceFromPointToFiniteLine(laneLine [i], laneLine [i + 1], transform.position).magnitude;
//                    rankThisDist.Add(distanceFromLane, i);
//                }
//                waypoints.Enqueue(laneLine [rankThisDist.Top() + 1]);
//                if (rankThisDist.Top() < laneLine.Count + 2) {
//                    waypoints.Enqueue(laneLine [rankThisDist.Top() + 2]);
//                }
//            }


        }

        //      Minion aggro priority: (which also works for towers if we ignore 2,3,4,5, and 7)
        //  1 A An enemy forger designated by a call for help from an allied forger. (Enemy forger attacking an Allied forger)
        //  2 B     An enemy minion designated by a call for help from an allied forger. (Enemy minion attacking an Allied forger)
        //  3 C         An enemy minion designated by a call for help from an allied minion. (Enemy minion attacking an Allied minion)
        //  4 D             An enemy turret designated by a call for help from an allied minion. (Enemy turret attacking an Allied minion)
        //  5 E                 An enemy forger designated by a call for help from an allied minion. (Enemy forger attacking an Allied minion)
        //  6 F                     The closest enemy minion.
        //  7 F                     The closest enemy turret.
        //  8 F                     The closest enemy forger.

        protected override int GetEnemyPriority (Unit unit)
        {
            int priority = 10;
            switch (unit.Kind) {
            case UnitKind.Minion:
                priority = 6;
                break;
            case UnitKind.Tower:
                priority = 7;
                break;
            case UnitKind.Forger:
                priority = 8;
                break;
            default:
                priority = 9;     // this is a unit in aggro range. 
                //                  AddTarget(9,unit);    
                break;
            }
            return priority * 100 + Mathf.CeilToInt(me.GetDistance(unit));
        }

        private Option<Vector3> GetNearestTowerPosition() {
            IEnumerable<Unit> towers = (IEnumerable<Unit>)UnityEngine.Object.FindObjectsOfType<Unit>().Where(unit => {
                return (unit as Unit).Kind == UnitKind.Tower && unit.TeamQualifiesAs(TeamQualifier.EnemyTeam, me.Team);
                //              return true;
            }).OrderBy( unit => unit.GetDistance( me ));

            return Option<Vector3>.Some( towers.First<Unit>().transform.position );

        }

        [System.Obsolete]
        private bool GetTarget( out Unit result) {

            result = default(Unit);

            if (result != default(Unit)) {
                return true;
            } else {
                return false;
            }

        }

        public void ForgetMe (Unit unit) {
            nextTarget.Remove(unit); 
        }

        protected bool InLane {
            get {

                for (int i = 0; i < laneLine.Count - 1; i++) {
                    float distanceFromLane = Utils.DistanceFromPointToFiniteLine(laneLine [i], laneLine [i + 1], transform.position).magnitude;

                    if (distanceFromLane < MINION_TETHER_DISTANCE * Constants.TO_UNITY_UNITS)
                        return true;
                }
                return false;
            }
        }

    }
}
