﻿using System;
using UnityEngine;
using UnityEngine.Networking;

using core;
using core.utility;

namespace controllers
{
    /// <summary>
    /// For projectiles that should continue to follow a given target until
    /// it has either hit them or they've died. If the target dies before 
    /// the projectile will move towards their last alive position.
    /// </summary>
    class TargetedProjectileController : ProjectileController {
        [SyncVar]
        NetworkInstanceId targetID;

        [SerializeField]
        uint localTargetID; 

        [SerializeField]
        bool lostTarget;
        [SerializeField]
        Unit target;



        [SerializeField]
        public GameObject DeadEndPrefab;

        new public Unit Target {
            get { return target; }
            set {
                target = value;
                if (target != null) {
                    targetLocation = target.transform.position;
                    targetID = target.netId;
                    localTargetID = targetID.Value;
                }
            }
        }



        void Start() {
        }

        void OnCollisionEnter( Collision bonk )
        {
            Unit colUnit = bonk.gameObject.GetComponent<Unit>();
        
            if( colUnit == null || colUnit != target )
                return;
        
            if (OnHitUnit != null)
                OnHitUnit(this, colUnit);
        }

        public override void BreakMe ()
        {
            if (DeadEndPrefab != null) {
                #if UNITY_EDITOR
                Instantiate(DeadEndPrefab, 
                    transform.position, Quaternion.Euler(Vector3.zero), // Might want to move to an "impact point"

                    managers.GameManager.Instance.corpseRoot);   // Tough call between corpse or projectile
                #else
                Instantiate(DeadEndPrefab, 
                    transform.position, Quaternion.Euler(Vector3.zero), // Might want to move to an "impact point"
                    null);
                
                #endif
            }
            base.BreakMe();
        }

        protected override void LostTarget ()
        {
            lostTarget = true;
        }

        protected override void ReachedTarget ()
        {
            if (OnHitTarget != null) {
                OnHitTarget(this, Source, target);
            } else {
                BreakMe();
            }
        }

        protected override void EachTick ()
        {
            // TODO Once we bring this to the net code, make sure we don't bork position data.
            if (localTargetID != targetID.Value) {
                localTargetID = targetID.Value;
                var gob = ClientScene.FindLocalObject(targetID);
                if (gob != null) {
                    target = gob.GetComponent<Unit>();

                }
            }
        }

        protected override bool TargetIsLost {
            get {
                return target == null || target.IsDead;
            }
        }

        protected override void TargetInfo ()
        {
            targetLocation = target.transform.position;
            targetPosition = target.Position;
            targetRadius = target.Radius;
        }

        protected override Vector3 getTargetLocation ()
        {
            return target.transform.position;
        }

        protected override float getTargetRadius ()
        {
            return target.Radius;
        }

        public override void SetTarget(Unit unit, bool StopWhenUnitTargetLost = true) {
            this.StopWhenTargetLost = StopWhenUnitTargetLost;
            Target = unit;
        }


        /*
        void Update () {
            EachTick();

            if (TargetIsLost) {
                LostTarget();
            } else {
                TargetInfo();
            }

            transform.position = Vector3.MoveTowards(transform.position, targetLocation, Time.deltaTime * Velocity * Constants.TO_UNITY_UNITS);

            targetPosition = Utils.PlanarPoint(targetLocation);

            if (TargetIsLost) {
                if (StopWhenTargetLost || Utils.CollidesWith(Position, Radius, targetPosition, targetRadius)) {
                    if (OnStop != null) {
                        OnStop(this);
                    } else {
                        // Should default to this even if we have no OnHit action.
                        BreakMe();
                    }
                }
            } else if (Utils.CollidesWith(Position, Radius, target.Position, target.Radius)) {
                ReachedTarget();
            }


        }
        */
    }
}   // namespace