﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ProjectileDressing {

    [SerializeField]
    public string Name;
    [SerializeField]
    public GameObject DressingPrefab;
	
}
