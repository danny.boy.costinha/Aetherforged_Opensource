﻿using UnityEngine;

namespace graphics
{
  [RequireComponent(typeof(Animator))]
  public class Wielding : MonoBehaviour
  {
    Animator animator;
//    public Transform GetBoneTransform (HumanBodyBones Hand.r);
    public GameObject righthandwield;
    public GameObject righthand;
    public GameObject lefthandwield;
    public GameObject lefthand;

    void Start ()
    {
      if( righthandwield != null && righthand != null )
        righthandwield.transform.parent = righthand.gameObject.transform;

      if( lefthandwield != null && lefthand != null )
        lefthandwield.transform.parent  = lefthand.gameObject.transform;
    }
  }
}