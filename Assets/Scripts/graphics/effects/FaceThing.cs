﻿using UnityEngine;
using System.Collections;

public class FaceThing : MonoBehaviour {

	public Transform ThingToLookAt;

	void Start() 
	{
		//transform.Rotate( 180,0,0 );
	}

	void Update() 
	{
		Vector3 v = ThingToLookAt.transform.position - transform.position;
		v.x = v.z = 0.0f;
		v.x = v.y = 0.0f;
		transform.LookAt( ThingToLookAt.transform.position - v ); 
		transform.Rotate(0,180,0);
	}
}
