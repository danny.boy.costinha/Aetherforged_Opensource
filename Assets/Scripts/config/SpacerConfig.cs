﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace config {
    public class SpacerConfig {

        string name;

        Sprite image;

        Color color;

        bool label;

        GameObject inGameSpacer;

        public SpacerConfig(string name, Sprite image, Color colour, bool showLabel) {
            this.name = name;
            this.image = image;
            this.color = colour;
            this.label = showLabel;
        }

        public void SetUp(GameObject spacer) {
            this.inGameSpacer = spacer;

            var imageComponent = inGameSpacer.GetComponent<Image>();

            imageComponent.sprite = image;
            imageComponent.color = color;

            // Label
            inGameSpacer.GetComponentInChildren<Text>().text = (label)? name : "";
        }

    }
}