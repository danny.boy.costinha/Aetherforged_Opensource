﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuErrorScript : MonoBehaviour {

    private object message = "";

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Find("ErrorLabel").gameObject.GetComponent<Text>().text = message.ToString();
    }

    public void Throw(object message) {
        gameObject.SetActive(true);
        this.message = message;
    }
}
