﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContentPanelManager : MonoBehaviour {

    public GameObject StartingPanel;
    public float FadeTime = 0.5f;

    private GameObject _currentPanel;
    private GameObject _fadeToPanel;
    private bool fading = false;
    private float EndFadeTime;

	// Use this for initialization
	void Start () {
        _currentPanel = StartingPanel;
        _currentPanel.SetActive(true);
	}

    private void Update()
    {
        if(fading)
        {
            FadeOldPanelToNewPanel(_currentPanel, _fadeToPanel);
        }
    }

    public void FadeToPanel(GameObject panel)
    {
        fading = true;
        _fadeToPanel = panel;
        _fadeToPanel.GetComponent<CanvasRenderer>().SetAlpha(0.01f);
        _fadeToPanel.SetActive(true);
        EndFadeTime = FadeTime * Time.deltaTime;
    }

    public void FadeOldPanelToNewPanel(GameObject oldpanel, GameObject newPanel)
    {
        //fade out
        oldpanel.GetComponent<CanvasRenderer>().SetAlpha(Mathf.Lerp(1.0f, 0.01f, EndFadeTime));
        newPanel.GetComponent<CanvasRenderer>().SetAlpha(Mathf.Lerp(0.01f, 1.00f, EndFadeTime));

        if(Time.deltaTime >= EndFadeTime)
        {
            oldpanel.SetActive(false);
            _currentPanel = newPanel;
            fading = false;
        }
    }
}
