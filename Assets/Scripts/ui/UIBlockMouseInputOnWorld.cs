﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ui
{
    public class UIBlockMouseInputOnWorld : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public static bool CanInteract = true;

        public void OnPointerEnter(PointerEventData eventData)
        {
            CanInteract = false;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            CanInteract = true;
        }
    }
}
