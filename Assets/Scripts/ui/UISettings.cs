﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.Networking;

using core.forger;
using core;
using controllers;

using managers;
using UnityEngine.AI;

public class UISettings : MonoBehaviour
{
    public Dropdown forgerDropdown;
    public Toggle fullscreenToggle;
    public Toggle FPSToggle;
    public Toggle PINGToggle;
    public Toggle ItemsToggle;
    public Toggle StatsToggle;
    public Dropdown resolutionDropdown;
    public Dropdown textureQualityDropdown;
    public Dropdown antialiasingDropdown;
    public Dropdown vSyncDropdown;
    public Slider masterVolumeSlider;
    public Slider musicVolumeSlider;
    public Slider effectsVolumeSlider;
    public Slider ambienceVolumeSlider;
    public Slider voiceVolumeSlider;
    public Slider announcerVolumeSlider;

    public Button Host;
    public Button Join;
    public Button DefaultJoin;
    public InputField PlayerName;
    public InputField IPAddress;
    public Slider edgeWidth;
    public Text edgeWidthDisplay;
    public Slider edgeSpeed;
    public Text edgeSpeedDisplay;
    public Slider arrowSpeed;
    public Text arrowSpeedDisplay;

    public Slider panSensitivity;
    public Text panSensitivityDisplay;
    public Slider zoomSpeed;
    public Text zoomSpeedDisplay;
    private string edgeWidthDisplayText;
    private string edgeSpeedDisplayText;
    private string arrowSpeedDisplayText;
    private string panSensitivityDisplayText;
    private string zoomSpeedDisplayText;


    public List<string> forgers;
    PlayerKnob player;

    void OnEnable()
    {
        if (SettingManager.Ready)
        {
            player = UnitManager.Instance.LocalKnob;
            
            //Set the playername on startup
            UnitManager.Instance.LocalKnob.PlayerName = PlayerPrefs.GetString("PlayerName");
            UnitManager.Instance.LocalKnob.CmdSetName(UnitManager.Instance.LocalKnob.PlayerName);

            //Set the slider position by playerprefs
            masterVolumeSlider.value = PlayerPrefs.GetFloat("Master Volume");

            musicVolumeSlider.value = PlayerPrefs.GetFloat("Music Volume");

            effectsVolumeSlider.value = PlayerPrefs.GetFloat("SFX Volume");

            ambienceVolumeSlider.value = PlayerPrefs.GetFloat("Ambience Volume");

            voiceVolumeSlider.value = PlayerPrefs.GetFloat("Voice Volume");

            announcerVolumeSlider.value = PlayerPrefs.GetFloat("Announcer Volume");

            fullscreenToggle.isOn = IntToBool(PlayerPrefs.GetInt("fullscreen"));
            FPSToggle.isOn = IntToBool(PlayerPrefs.GetInt("FPS"));
            PINGToggle.isOn = IntToBool(PlayerPrefs.GetInt("PING"));
            ItemsToggle.isOn = IntToBool(PlayerPrefs.GetInt("ItemsPanel"));
            StatsToggle.isOn = IntToBool(PlayerPrefs.GetInt("StatsPanel"));

            //Set camera options to saved values
            edgeWidth.value = PlayerPrefs.GetFloat("edgeWidth");
            edgeWidthDisplay.GetComponent<UnityEngine.UI.Text>().text = Mathf.Round(100*(PlayerPrefs.GetFloat("edgeWidth"))).ToString() + "%";
            arrowSpeed.value = PlayerPrefs.GetFloat("arrowSpeed");
            arrowSpeedDisplay.GetComponent<UnityEngine.UI.Text>().text = PlayerPrefs.GetFloat("arrowSpeed").ToString();
            panSensitivity.value = PlayerPrefs.GetFloat("panSensitivity");
            panSensitivityDisplay.GetComponent<UnityEngine.UI.Text>().text = Mathf.Round(100*(PlayerPrefs.GetFloat("panSensitivity"))).ToString();
            edgeSpeed.value = PlayerPrefs.GetFloat("edgeSpeed");
            edgeSpeedDisplay.GetComponent<UnityEngine.UI.Text>().text = PlayerPrefs.GetFloat("edgeSpeed").ToString();
            zoomSpeed.value = PlayerPrefs.GetFloat("zoomSpeed");
            zoomSpeedDisplay.GetComponent<UnityEngine.UI.Text>().text = PlayerPrefs.GetFloat("zoomSpeed").ToString();

            //Camera options
            edgeWidth.onValueChanged.AddListener((amount) => 
            { 
                CameraController.instance.floatUpdate("EdgeWidth", amount);
                edgeWidthDisplay.GetComponent<UnityEngine.UI.Text>().text = (Mathf.Round(amount*100) + "%");
                SettingManager.Instance.OnCameraSettingChange("edgeWidth", amount);
            });
            arrowSpeed.onValueChanged.AddListener((amount) => 
            {
                CameraController.instance.floatUpdate("ArrowSpeed", amount);
                arrowSpeedDisplay.GetComponent<UnityEngine.UI.Text>().text = (amount + "");
                SettingManager.Instance.OnCameraSettingChange("arrowSpeed", amount);
            });
            panSensitivity.onValueChanged.AddListener((amount) => 
            {
                CameraController.instance.floatUpdate("panSensitivity", amount);
                panSensitivityDisplay.GetComponent<UnityEngine.UI.Text>().text = (Mathf.Round(amount*100) + "");
                SettingManager.Instance.OnCameraSettingChange("panSensitivity", amount);
            });
            edgeSpeed.onValueChanged.AddListener((amount) => 
            { 
                CameraController.instance.floatUpdate("EdgeSpeed", amount);
                edgeSpeedDisplay.GetComponent<UnityEngine.UI.Text>().text = (amount + "");
                SettingManager.Instance.OnCameraSettingChange("edgeSpeed", amount);
            });
            zoomSpeed.onValueChanged.AddListener((amount) => 
            { 
                CameraController.instance.floatUpdate("zoomSpeed", amount);
                zoomSpeedDisplay.GetComponent<UnityEngine.UI.Text>().text = (amount + "");
                SettingManager.Instance.OnCameraSettingChange("zoomSpeed", amount);
            });

            //Graphics Options
            fullscreenToggle.onValueChanged.AddListener((bool onOrOff) => { SettingManager.Instance.OnFullScreenToggle(onOrOff); });
            FPSToggle.onValueChanged.AddListener((bool onOrOff) => { SettingManager.Instance.OnFPSToggle(onOrOff); });
            PINGToggle.onValueChanged.AddListener((bool onOrOff) => { SettingManager.Instance.OnPINGToggle(onOrOff); });
            ItemsToggle.onValueChanged.AddListener((bool onOrOff) => { SettingManager.Instance.OnItemsPanelToggle(onOrOff); });
            StatsToggle.onValueChanged.AddListener((bool onOrOff) => { SettingManager.Instance.OnStatsPanelToggle(onOrOff); });

            resolutionDropdown.onValueChanged.AddListener((int number) => { SettingManager.Instance.OnResolutionChange(number); });
            textureQualityDropdown.onValueChanged.AddListener((int quality) =>
            {
                SettingManager.Instance.OnTextureQualityChange(quality);
            });

            antialiasingDropdown.onValueChanged.AddListener((int number) => { SettingManager.Instance.OnAntialiasingChange(number); });
            vSyncDropdown.onValueChanged.AddListener( (int vSyncNumber) => {
                GameSettings.vSync = vSyncNumber;
                SettingManager.Instance.OnVSyncChange(vSyncNumber);
            });

            //Audio Options
            masterVolumeSlider.onValueChanged.AddListener((amount) => { 
                SettingManager.Instance.OnVolumeSliderChange(amount, ref GameSettings.masterVolume, "Master Volume");
            });
            musicVolumeSlider.onValueChanged.AddListener((amount) => {
                SettingManager.Instance.OnVolumeSliderChange(amount, ref GameSettings.musicVolume, "Music Volume");
            });
            effectsVolumeSlider.onValueChanged.AddListener((amount) => {
                SettingManager.Instance.OnVolumeSliderChange(amount, ref GameSettings.effectsVolume, "SFX Volume");
            });
            ambienceVolumeSlider.onValueChanged.AddListener((amount) => {
                SettingManager.Instance.OnVolumeSliderChange(amount, ref GameSettings.ambienceVolume, "Ambience Volume");
            });
            voiceVolumeSlider.onValueChanged.AddListener((amount) => {
                SettingManager.Instance.OnVolumeSliderChange(amount, ref GameSettings.voiceVolume, "Voice Volume");
            });
            announcerVolumeSlider.onValueChanged.AddListener((amount) => {
                SettingManager.Instance.OnVolumeSliderChange(amount, ref GameSettings.announcerVolume, "Announcer Volume");
            });

            forgerDropdown.options.Clear();
            forgers = UnitManager.Instance.GetListOfImprints();
            foreach(string name in forgers){
                forgerDropdown.options.Add(new Dropdown.OptionData(name));
            }

            textureQualityDropdown.value = PlayerPrefs.GetInt("QualityIndex");
            antialiasingDropdown.value = PlayerPrefs.GetInt("AntialiasingIndex");
            vSyncDropdown.value = PlayerPrefs.GetInt("VSyncIndex");
            
            foreach (Resolution resolution in SettingManager.Instance.resolutions)
            {
                resolutionDropdown.options.Add(new Dropdown.OptionData(resolution.ToString()));
            }
            resolutionDropdown.value = PlayerPrefs.GetInt("ResolutionIndex");

            PlayerName.onValueChanged.AddListener( (string playerName) => {
                PlayerPrefs.SetString("PlayerName", playerName);
                if (UnitManager.Ready && UnitManager.Instance.LocalKnob != null)
                {
                    UnitManager.Instance.LocalKnob.PlayerName = PlayerName.text;
                    UnitManager.Instance.LocalKnob.CmdSetName(playerName);
                }
            });
            PlayerName.text = PlayerPrefs.GetString("PlayerName", "<Name Not Found>");

            IPAddress.onValueChanged.AddListener((string ipAddress) => PlayerPrefs.SetString("IPAddress", IPAddress.text));
            IPAddress.text = PlayerPrefs.GetString("IPAddress");
        }

        if (UnitManager.Ready && UnitManager.Instance.LocalForger) {
            forgerDropdown.value = forgers.IndexOf(UnitManager.Instance.LocalForger.KitName);
            UnitManager.Instance.LocalKnob.PlayerName = PlayerName.text;
        }
    }

    public void SpawnForger()
    {
        var forgerName = forgers[forgerDropdown.value];
        UnitManager.Instance.LocalKnob.CmdSwapForger_DEBUG(forgerName);
    }
    
    public void ClientConnect()
    {
        var ipAddress = IPAddress.text;

        AetherForgedNetworkManager.Instance.networkAddress = ipAddress;
        AetherForgedNetworkManager.Instance.StartClient();
    }

    public void ClientDefaultConnect()
    {
        var ipAddress = "aetherforged.mynetgear.com";

        AetherForgedNetworkManager.Instance.networkAddress = ipAddress;
        AetherForgedNetworkManager.Instance.StartClient();
    }

    public void ClientLocalConnect()
    {
        var ipAddress = "localhost";

        AetherForgedNetworkManager.Instance.networkAddress = ipAddress;
        AetherForgedNetworkManager.Instance.StartClient();
    }

    public void Disconnect()
    {
        AetherForgedNetworkManager.Instance.StopClient();
    }

    public void HostGame()
    {
        var hostButtonText = Host.GetComponentInChildren<Text>();
        if(NetworkServer.active == false && NetworkClient.active == false){
            AetherForgedNetworkManager.Instance.StartHost();
            hostButtonText.text = "Stop";
        }
        else{
            AetherForgedNetworkManager.Instance.StopHost();
            hostButtonText.text = "Host";
        }
    }

    void OnDisable()
    {

        //Graphics Options
        fullscreenToggle.onValueChanged.RemoveAllListeners();
        resolutionDropdown.onValueChanged.RemoveAllListeners();
        textureQualityDropdown.onValueChanged.RemoveAllListeners();
        antialiasingDropdown.onValueChanged.RemoveAllListeners();
        vSyncDropdown.onValueChanged.RemoveAllListeners();

        //Audio Options
        masterVolumeSlider.onValueChanged.RemoveAllListeners();
        musicVolumeSlider.onValueChanged.RemoveAllListeners();
        effectsVolumeSlider.onValueChanged.RemoveAllListeners();
        ambienceVolumeSlider.onValueChanged.RemoveAllListeners();
        voiceVolumeSlider.onValueChanged.RemoveAllListeners();
        announcerVolumeSlider.onValueChanged.RemoveAllListeners();
    }

    public bool IntToBool(int value)
    {
        if(value == 0){
            return false;
        }
        else{
            return true;
        }
    }

    public void DoQuit()
    {
        GameManager.Instance.ExitApp();
    }
}
