﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
using System.Collections;
using System;


using ui;
using core;
using managers;
using core.forger;
using controllers;
using controllers.ai;
using core.utility;
using itemdata = feature.ItemLoader;

namespace controllers
{
    
    public class UIPlayerHUD : MonoBehaviour
    {
        
        [SerializeField]
        uint localForgerID;

        public string PlayerName;

        public Team MyTeam;

        [SerializeField]
        Forger forger;
        public Forger ControlForger
        {
            get
            {
                //                if (forger == null) {
                //                    var gob = ClientScene.FindLocalObject(forgerID);
                //                    forger = gob.GetComponent<Forger>();
                //                    intellect = forger.GetComponent<ForgerAI>();
                //                    kit = forger.GetComponentInChildren<Kit>();
                //                }

                return forger;
            }
            set
            {
                forger = value;


                intellect = forger.GetComponent<ForgerAI>();
                var holder = forger.GetComponentInChildren<KitHolder>();
                if (holder != null)
                {
                    kit = holder.kit;
                }

                //Debug.Assert(kit != null);
            }
        }


        float localGameTime
        {
            get {
                return GameTime.time;
            }
        }

        public RawImage LiveMap
        {
            get { return mapClockPanel.liveMinimap; }
        }

        private bool MouseOverMap;

        ForgerAI intellect;
        //      [SerializeField]
        CameraController playerCamCon;  // disambiguation with 'camera'
        int abilityToCastIndex;

        private Castable abilityToCast;
        public Castable AbilityToCast
        {
            get { return abilityToCast; }
        }
        public bool AbilitySetToCast
        {
            get { return (abilityToCast != null); }
            set
            { // Kinda janky approach, but ehhhhh it works. 
                if (!value)
                {
                    abilityToCast = null;
                }
            }
        }
        public void ResetAbilitySetToCast()
        {
            abilityToCast = null;
        }


        GameObject pointer;
        GameObject miniMapCamera;
        MinimapController miniMapCon;
        Kit kit;

        bool kitReady
        {
            get
            {
                if (kit == null && forger != null)
                {
                    var holder = forger.GetComponentInChildren<KitHolder>();
                    kit = holder.kit;
                }
                return (kit != null);
            }
        }

        [SerializeField]
        BottomPanel bottomPanel = new BottomPanel();
        [SerializeField]
        StatsClockPanel clockPanel = new StatsClockPanel();
        [SerializeField]
        LocalStatsPanel localStatsPanel = new LocalStatsPanel();
        [SerializeField]
        MiniMapPanel mapClockPanel = new MiniMapPanel();

        [SerializeField]
        private bool attackCursor;

        void Start()
        {
            //            forger = GetComponent<Forger>();
            //            intellect = forger.GetComponent<ForgerAI>();
            //            kit = forger.GetComponentInChildren<Kit>();
            //
            //            Debug.Assert(kit != null);
            /*
            if (isLocalPlayer)
            {
                pointer = Instantiate(Resources.Load("Pointer") as GameObject);

                HidePointer();

                miniMapCamera = Instantiate(Resources.Load("MiniMapCamera") as GameObject);
                miniMapCamera.transform.SetParent(gameObject.transform);

                miniMapCon = miniMapCamera.GetComponent<MinimapController>();

                var preferredName = PlayerPrefs.GetString("PlayerName", "<Name Not Found>");
                StartCoroutine(GameManager.Instance.DoDelay(() =>
                {
                    CmdSetName(preferredName);
                    return 0f;
                }, 0.1f));

            }
            */

            InitPanels();

        }


        void Update()
        {
            
            if (NetworkClient.active)
            {
                
                if (UnitManager.Instance.LocalForger != forger )
                {
                    forger = UnitManager.Instance.LocalForger;
                }


                #region UI update

                if (NetworkClient.active)
                {
                    //print(GameObject.Find("UICanvas/BottomPanel").GetComponent<EventSystem>().IsPointerOverGameObject());

                    var gob = GameObject.Find("HudCanvas");
                    // If there is not a HUD, do not try to update the HUD
                    if (gob != null)
                    {
                        if (bottomPanel.panel == null || mapClockPanel.panel == null)
                        {
                            InitPanels();
                        }
                        else if (bottomPanel.panel != null)
                        {
                            PanelsUpdate();
                        }
                    }
                }


                #endregion

            }
        }

        void barFill(Image bar, float ratio)
        {
            if (bar.type == Image.Type.Filled)
            {
                bar.fillAmount = ratio;
            }
            else
            {
                var barTransform = bar.GetComponent<RectTransform>();
                barTransform.anchorMax = new Vector2(forger.HPRatio, 1);
            }
        }


        void HidePointer()
        {
            pointer.SetActive(false);
        }

        void PlacePointer(Vector3 point)
        {
            pointer.SetActive(false);
            pointer.SetActive(true);
            // Wooo! Fixed. when we fix the pointer pivot point, fix this too. 
            pointer.transform.position = point;
        }



        public Option<Unit> GetClickedOnUnit(Click click)
        {
            return ClickRaycast(click == Click.Target ? 0 : 1, Utils.UNIT_LAYER)
                .AndThen<Unit>(x =>
                {

                    var u = x.transform.gameObject.GetComponent<Unit>();

                    return Option<Unit>.Some(u);
                });
        }

        public Option<Vector3> GetClickedOnPoint(Click click)
        {
            return ClickRaycast(click == Click.Target ? 0 : 1, Utils.GROUND_LAYER)
                    .Map<Vector3>(x => x.point);
        }

        public Option<Vector3> GetMouseOnPoint()
        {
            return GetMouseRaycast(Utils.GROUND_LAYER)
                    .Map<Vector3>(x => x.point);
        }

        private Option<RaycastHit> ClickRaycast(int mouseButton, int layer)
        {
            if (playerCamCon)
            {
                if (Input.GetMouseButton(mouseButton))
                {
                    Ray clickRay = playerCamCon.camera.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;

                    if (Physics.Raycast(clickRay, out hit, 200, 1 << layer))
                    {
                        return Option<RaycastHit>.Some(hit);
                    }
                }
            }

            return Option<RaycastHit>.None();
        }

        public Option<RaycastHit> GetMouseRaycast(int layer)
        {
            if (playerCamCon)
            {
                Ray clickRay = playerCamCon.camera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(clickRay, out hit, 500, LayerMask.GetMask("Ground")))
                {
                    return Option<RaycastHit>.Some(hit);
                }
            }

            return Option<RaycastHit>.None();
        }


        // DO WE NEED This? We have 
        public bool TryGetPlanarPoint(out Vector2 position, int layer = Utils.GROUND_LAYER)
        {
            if (playerCamCon)
            {
                Ray clickRay = playerCamCon.camera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(clickRay, out hit, 500, 1 << layer))
                {
                    position = Utils.PlanarPoint(hit.point);
                    return true;
                }
            }
            position = Vector2.one;
            return false;
        }


        [Serializable]
        public class BottomPanel : UIPanel
        {

            public Image healthBar;

            public Text[] cooldownAbility;

            public Image[] iconsAbility;

            public Text[] hotKeyAbility;

            public Button[] levelUpAbility;

            public Image[] abilityLevelAbility;


            public Image resourceBar;

            public Text healthText;

            public Text resourceText;

            public Image experienceBar;

            public Image moneyBar;

            public Text moneyText;

            public Text saltAmountDebug;

            public Text levelNumber;

        }

        [Serializable]
        public class StatsClockPanel : UIPanel
        {

            public Text ingameclock;

            public Text pingReadout;

            public Text killNumber;
            //
            public Text helpNumber;
            //
            public Text deadNumber;
            //
            public Text creepNumber;

        }
        [Serializable]
        public class LocalStatsPanel : UIPanel
        {
            public Text powerStatText;
            public Text armorStatText;
            public Text mrStatText;
            public Text attackDamageStatText;
            public Text hasteStatText;
            public Text movementSpeedStatText;
        }

        [Serializable]
        public class MiniMapPanel : UIPanel
        {

            public Text ingameclock;

            public Text kdaText;

            public Text csText;

            public RawImage liveMinimap;
        }


        public void PanelsUpdate()
        {

            if (forger != null)
            {
                barFill(bottomPanel.healthBar, forger.HPRatio);



                bottomPanel.healthText.text = forger.HPPrettified;


                barFill(bottomPanel.experienceBar, forger.XPRatio);

                barFill(bottomPanel.moneyBar, forger.SaltRatio);

                bottomPanel.moneyText.text = string.Format("{0}", forger.Salt);


                bottomPanel.levelNumber.text = string.Format("{0}", forger.Level);


                if (forger != null && forger.Resource != null)
                {
                    barFill(bottomPanel.resourceBar, forger.Resource.Ratio);

                    // TODO 2017 Jun 12 We'll have to think about setting the whole image, since gradient inner fire looks noice!
                    //                    GameObject.Find("ResourcePanel/ResourceBar").GetComponent<Image>().color = forger.Resource.Color;

                    bottomPanel.resourceText.text = forger.Resource.Prettified;
                }

                {   // Passive
                    if (kitReady && kit.passives.Length > 0 && bottomPanel.cooldownAbility[0] != null)
                    {
                        bottomPanel.cooldownAbility[0].text =
                            ControlForger.CooldownInSlot(0).Prettify();



                        // TODO attach the below line to a Tooltip
                        //                            kit.Passives[i].GetSubtextPrettified(forger);

                        {
                            
                            var tempSprite = kit.passives[0].Icon;
                            if (tempSprite == null)
                            {
                                var iconAddress = string.Format("Images/AbilityIcons/" + kit.Name + "/" + kit.Name + 0);
                                tempSprite = Resources.Load<Sprite>("Images/AbilityIcons/" + kit.Name + "/" + kit.Name + 0);
                            }
                            if (tempSprite == null)
                            {
                                tempSprite = Resources.Load<Sprite>("Images/AbilityIcons/" + kit.Name + "/Finals/" + "Passive");
                            }
                            if (tempSprite == null)
                            {
                                Debug.LogErrorFormat("NO Sprite Found");
                            }
                            bottomPanel.iconsAbility[0].sprite = tempSprite;
                        }
                    }
                }


                for (int i = 1; i <= 4; i++)
                {
                    string abilityFindAffix = i == 1 ? "Q" : i == 2 ? "W" : i == 3 ? "E" : i == 4 ? "R" : "";
                    if (bottomPanel.cooldownAbility[i] != null)
                    {
                        Castable ability;
                        if (kitReady && abilityFindAffix != "" && forger.TryActiveInSlot(i, out ability))
                        {
                            bottomPanel.cooldownAbility[i].text =
                                ControlForger.CooldownInSlot(i).Prettify();
                            bottomPanel.hotKeyAbility[i].text =
                            KeyConfig.Instance.ShowKey(ICE.AbilityOneForger + i - 1);


                            // TODO attach the below line to a Tooltip
                            //                            kit.Abilities[i].GetSubtextPrettified(forger);

                            {

                                var tempSprite = ability.Icon;

                                if (tempSprite == null)
                                {
                                    var iconAddress = string.Format("Images/AbilityIcons/" + kit.Name + "/" + kit.Name + i);
                                    tempSprite = Resources.Load<Sprite>("Images/AbilityIcons/" + kit.Name + "/" + kit.Name + i);
                                }
                                if (tempSprite == null)
                                {
                                    tempSprite = Resources.Load<Sprite>("Images/AbilityIcons/" + kit.Name + "/Finals/" + abilityFindAffix);
                                }
                                if (tempSprite == null)
                                {
                                    tempSprite = Resources.Load<Sprite>("Images/Avatars/" + "avatar_ainsley");
                                    // TODO move this warning to the imprint editor tool
                                    //Debug.LogWarningFormat("NO Sprite Found for {0} {1}", kit.Name, abilityFindAffix);
                                }
                                bottomPanel.iconsAbility[i].sprite = tempSprite;
                            }

                            bottomPanel.levelUpAbility[i].gameObject.SetActive(forger.CanSkillUp(i));

                            if (bottomPanel.abilityLevelAbility[i] != null)
                            {
                                bottomPanel.abilityLevelAbility[i].fillAmount = forger.AbilityLevelBarFillAmount(i);
                            }

                        }
                        else
                        {
                            bottomPanel.cooldownAbility[i].text = "";
                        }
                    }
                }





                /*
                clockPanel.killNumber.text = string.Format("{0}", forger.Kills);


                clockPanel.helpNumber.text = string.Format("{0}", forger.Assists);


                clockPanel.deadNumber.text = string.Format("{0}", forger.Deaths);


                clockPanel.creepNumber.text = string.Format("{0}", forger.Creeps);

                clockPanel.ingameclock.text = string.Format(
                    "{0}:{1:00}:{2:00}"
                    , Mathf.FloorToInt(SyncedTime / (60 ^ 2))
                    , Mathf.FloorToInt(SyncedTime / 60f % 60)
                    , Mathf.RoundToInt(SyncedTime % 60f));
                */
                if (mapClockPanel.panel != null)
                {
                    /*
                    mapClockPanel.ingameclock.text = string.Format(
                        "{0}:{1:00}:{2:00}"
                        , Mathf.FloorToInt(localGameTime / (60 * 60))
                        , Mathf.FloorToInt(localGameTime / 60f % 60)
                        , Mathf.RoundToInt(localGameTime % 60f));
                    */

                    mapClockPanel.kdaText.text = string.Format("{0}", forger.PrettyKDA);

                    mapClockPanel.csText.text = string.Format("{0:000}", forger.Creeps);

                    Ping pt = AetherForgedNetworkManager.PingOut;

                    //            if (pt.isDone) 
                    {   /*
                    Text pingNumber = mapClockPanel.pingReadout;
                    // Gets the return trip time. So simple. Who knew?
                    int pingRTT = AetherForgedNetworkManager.Instance.client.GetRTT();
                    pingNumber.text = string.Format("{0}", pingRTT);
                    // Do we want to put a delay on this?

                    {
                        var pingtime = pingRTT;
                        // Ping colors indigo <30 - green 30+ - yellow 50+ - 100+ red
                        if (pingtime < 30)
                        {
                            pingNumber.color = new Color(0.58f, 0.49f, 0.89f);
                        }
                        else if (pingtime < 50)
                        {
                            pingNumber.color = Color.green;
                        }
                        else if (pingtime < 100)
                        {
                            pingNumber.color = Color.yellow;
                        }
                        else
                        {
                            pingNumber.color = Color.red;
                        }
                    }
                    */
                    }
                }

                localStatsPanel.powerStatText.text = Mathf.Round(forger.TotalStats.Power).ToString();
                localStatsPanel.armorStatText.text = Mathf.Round(forger.TotalStats.Armor).ToString();
                localStatsPanel.mrStatText.text = Mathf.Round(forger.TotalStats.Resistance).ToString();
                localStatsPanel.attackDamageStatText.text = Mathf.Round(forger.TotalStats.AutoAttackDamage).ToString();
                localStatsPanel.hasteStatText.text = Mathf.Round(forger.TotalStats.Haste).ToString();
                localStatsPanel.movementSpeedStatText.text = Mathf.Round(forger.TotalStats.MovementSpeed).ToString();
            }

            if (mapClockPanel.panel != null)
            {
                mapClockPanel.ingameclock.text = string.Format(
                    "{0}:{1:00}:{2:00}"
                    , Mathf.FloorToInt(localGameTime / (60 * 60))
                    , Mathf.FloorToInt(localGameTime / 60f % 60)
                    , Mathf.RoundToInt(localGameTime % 60f));
            }
        }

        public void InitPanels()
        {
            bottomPanel.panel = GameObject.Find("HudCanvas/BottomPanel");
            clockPanel.panel = GameObject.Find("HudCanvas/TopRightStats");
            localStatsPanel.panel = GameObject.Find("HudCanvas/BottomPanel/StatsPanel");
            mapClockPanel.panel = GameObject.Find("HudCanvas/ClockStats");

            if (bottomPanel.panel != null)
            {

                var train = bottomPanel.panel.transform;

                // TODO optimize these finds. 

                bottomPanel.healthBar = GameObject.Find("HealthPanel/HealthBar").GetComponent<Image>();

                bottomPanel.resourceBar = GameObject.Find("ResourcePanel/ResourceBar").GetComponent<Image>();

                bottomPanel.healthText = GameObject.Find("HealthPanel/Text").GetComponent<Text>();

                // TODO 2017 Jun 12 We'll have to think about setting the whole image, since gradient inner fire looks noice!
                //                    GameObject.Find("ResourcePanel/ResourceBar").GetComponent<Image>().color = forger.Resource.Color;

                bottomPanel.resourceText = GameObject.Find("ResourcePanel/Text").GetComponent<Text>();

                bottomPanel.cooldownAbility = new Text[5];

                bottomPanel.hotKeyAbility = new Text[5];

                bottomPanel.iconsAbility = new Image[5];

                bottomPanel.levelUpAbility = new Button[5];

                bottomPanel.abilityLevelAbility = new Image[5];

                bottomPanel.cooldownAbility[0] = GameObjectUtil.FindGetComponent<Text>("PASSIVE" + "/CooldownTimer");

                bottomPanel.iconsAbility[0] = GameObjectUtil.FindGetComponent<Image>("PASSIVE" + "/ForgerPassive");


                for (int i = 1; i <= 4; i++)
                {
                    string abilityFindAffix = i == 1 ? "Q" : i == 2 ? "W" : i == 3 ? "E" : i == 4 ? "R" : "";
                    if (abilityFindAffix != "")
                    {
                        bottomPanel.cooldownAbility[i] = GameObjectUtil.FindGetComponent<Text>("Ability" + abilityFindAffix + "/CooldownTimer");
                        bottomPanel.hotKeyAbility[i] = GameObjectUtil.FindGetComponent<Text>("Ability" + abilityFindAffix + "/Subtext");
                        bottomPanel.iconsAbility[i] = GameObjectUtil.FindGetComponent<Image>("Ability" + abilityFindAffix + "/Image");
                        bottomPanel.levelUpAbility[i] = GameObjectUtil.FindGetComponent<Button>("Ability" + abilityFindAffix + "/LevelUp");
                        bottomPanel.abilityLevelAbility[i] = GameObjectUtil.FindGetComponent<Image>("Ability" + abilityFindAffix + "/AbilityLevel");
                        {
                            int slot = i;
                            bottomPanel.levelUpAbility[i].onClick.AddListener(() => {

                                ControlForger.player.CmdLevelUpAbility(slot);
                            });
                        }
                    }
                }

                bottomPanel.experienceBar = GameObjectUtil.FindGetComponent<Image>("ExperienceBar/Progress");

                bottomPanel.moneyBar = GameObjectUtil.FindGetComponent<Image>("SaltBar/Progress");

                bottomPanel.moneyText = GameObjectUtil.FindGetComponent<Text>("Money/Number");

                bottomPanel.levelNumber = GameObjectUtil.FindGetComponent<Text>("Level/Number");
            }
            if (clockPanel.panel != null)
            {
                clockPanel.killNumber = GameObjectUtil.FindGetComponent<Text>("TopRightStats/KillText");

                clockPanel.helpNumber = GameObjectUtil.FindGetComponent<Text>("TopRightStats/ASTText");

                clockPanel.deadNumber = GameObjectUtil.FindGetComponent<Text>("TopRightStats/DeathText");

                clockPanel.creepNumber = GameObject.Find("TopRightStats/CSText").GetComponent<Text>();

                clockPanel.pingReadout = GameObject.Find("TopRightStats/PingText").GetComponent<Text>();

                /*
                clockPanel.ingameclock = GameObject.Find("TopRightStats/IngameClock").GetComponent<Text>();
                /* */
            }
            if (localStatsPanel.panel != null)
            {
                localStatsPanel.powerStatText = localStatsPanel.Find<Text>("Stats/PlayerStats/StatBlockPanel/PowerStatBlock/PowerStatText");
                localStatsPanel.armorStatText = localStatsPanel.Find<Text>("Stats/PlayerStats/StatBlockPanel/ArmorStatBlock/ArmorStatText");
                localStatsPanel.mrStatText = localStatsPanel.Find<Text>("Stats/PlayerStats/StatBlockPanel/MRStatBlock/MRStatText");
                localStatsPanel.attackDamageStatText = localStatsPanel.Find<Text>("Stats/PlayerStats/StatBlockPanel/AttackDamageStatBlock/AttackDamageStatText");
                localStatsPanel.hasteStatText = localStatsPanel.Find<Text>("Stats/PlayerStats/StatBlockPanel/HasteStatBlock/HasteStatText");
                localStatsPanel.movementSpeedStatText = localStatsPanel.Find<Text>("Stats/PlayerStats/StatBlockPanel/MovementSpeedStatBlock/MovementSpeedStatText");
            }
            if (mapClockPanel.panel != null)
            {
                mapClockPanel.ingameclock = GameObject.Find("IngameClock").GetComponent<Text>();

                mapClockPanel.kdaText = GameObject.Find("ClockStats/KDAText").GetComponent<Text>();

                mapClockPanel.csText = GameObject.Find("ClockStats/CSText").GetComponent<Text>();

                mapClockPanel.liveMinimap = GameObject.Find("LiveMap").GetComponent<RawImage>();

                /*
                var trigger = mapClockPanel.liveMinimap.gameObject.AddComponent<MapTrigger>();
                trigger.knob = this;
                trigger.mapCamera = miniMapCamera.GetComponent<Camera>();


                EventTrigger.Entry entry = new EventTrigger.Entry();
                entry.eventID = EventTriggerType.PointerEnter;
                entry.callback.AddListener((data) =>
                {
                    MouseOverMap = true;
                });

                EventTrigger.Entry exit = new EventTrigger.Entry();
                exit.eventID = EventTriggerType.PointerExit;
                exit.callback.AddListener((data) =>
                {
                    MouseOverMap = false;
                });

                trigger.triggers.Add(entry);
                trigger.triggers.Add(exit);
                */
            }
        }


    }

}