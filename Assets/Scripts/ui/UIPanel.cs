﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ui
{
    [Serializable]
    public class UIPanel
    {
        public GameObject panel;

        public T Find<T>(string findMe)
        {
            var gob = panel.transform.Find(findMe);
            if (gob != null)
            {
                return gob.GetComponent<T>();
            }
            return default(T);
        }
 
    }
}