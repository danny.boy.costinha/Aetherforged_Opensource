﻿// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using UnityEngine.EventSystems;

// using UnityEngine.UI;

// public class UIPopup : MonoBehaviour {

// 	GameObject popupText;
// 	GameObject popupText1Deep;
// 	GameObject popupText2Deep;
// 	BoxCollider2D hoverArea;
// 	Vector2 hoverAreaSize;
// 	Vector2 hoverAreaOffset;
// 	EventTrigger eventTrigger;
// 	string gmName;
// 	string gmDescription;
// 	bool hoverStatus = false;

// 	void Start () 
// 	{
// 		gameObject.AddComponent(typeof(EventTrigger));

// 		AddEventTriggerListener(GetComponent<EventTrigger>(), EventTriggerType.PointerEnter, OnTriggerEntry);

// 		gmName = gameObject.name;
// 		//remove naming conventions from hierachy
// 		var stringsToRemove = new string[] {"Stat", "Block"};
// 		foreach(var s in stringsToRemove){
// 			gmName = gmName.Replace(s, string.Empty);
// 		}
// 		gmDescription = UIPopupDescriptions.Instance.PopupText(gmName);

// 		if(gameObject.GetComponentInParent<GridLayoutGroup>() != null){
// 			var currentDesc = transform.Find("UIPopupPanel/PopupDescription").GetComponent<Text>();
// 			currentDesc.text = gmDescription;
// 		}

// 		Transform popupTextTransform = transform.Find("UIPopupPanel");
// 		popupText = popupTextTransform.gameObject;

// 		var eventTriggerGameObject = popupText.AddComponent(typeof(EventTrigger));
// 		var eventTrigger = eventTriggerGameObject.GetComponent<EventTrigger>();
// 		AddEventTriggerListener(eventTriggerGameObject.GetComponent<EventTrigger>(), EventTriggerType.PointerEnter, OnTriggerEntry);
// 		AddEventTriggerListener(eventTriggerGameObject.GetComponent<EventTrigger>(), EventTriggerType.PointerExit, OnTriggerExit);

// 		if(transform.parent.name != "Stats"){
// 			GameObject newParent = GameObject.Find("HudCanvas/Popups");
// 			popupText.transform.SetParent(newParent.transform, true);	
// 		}	
// 	}

// 	public void AddEventTriggerListener(EventTrigger trigger, EventTriggerType eventType, System.Action<BaseEventData> callback){
// 		EventTrigger.Entry entry = new EventTrigger.Entry();
// 		entry.eventID = eventType;
// 		entry.callback = new EventTrigger.TriggerEvent();
// 		entry.callback.AddListener(new UnityEngine.Events.UnityAction<BaseEventData>(callback));
// 		trigger.triggers.Add(entry);
// 	}
	
// 	void OnTriggerEntry(BaseEventData eventData)
// 	{
// 		PointerEventData pointerEventData = (PointerEventData)eventData;
		
// 		if (hoverStatus == false){
// 			popupText.SetActive(true);

// 			hoverStatus = true;
// 		}
// 	}

// 	void Update()
// 	{
// 		//make if statment about the 1 deep popup needing to be set at a different location
// 		if (hoverStatus == true){
// 			if(transform.parent.parent.parent.name != "Popups"){
// 				popupText.transform.position = gameObject.transform.position;
// 			}
// 			else{
// 				popupText.transform.position = new Vector3(transform.parent.parent.position.x, transform.parent.parent.position.y, transform.parent.parent.position.z);
// 			}
// 		}
// 	}
// 	void OnTriggerExit(BaseEventData eventData)
// 	{
// 		if (hoverStatus == true){
// 			popupText.SetActive(false);
// 			hoverStatus = false;
// 		}
// 	}
// }
