﻿using UnityEngine;
using UnityEngine.UI;
using core;
using core.forger;
using System.Collections;
using System;

namespace ui
{
    [Obsolete]
	public class ForgerLevelIndicator : MonoBehaviour
	{

		public float forgerLevel;

		private GameObject self;
		private Text levelText;
		private string displayText;

		void Start ()
		{
			self = gameObject;
			levelText = self.GetComponent<Text> ();
		}
		
		// Update is called once per frame
		void Update ()
		{
			displayText = Convert.ToString (forgerLevel);
			levelText.text = displayText;
			// TODO: Tie this to a level-up event so it doesn't have to be in Update.
		}
	}
}