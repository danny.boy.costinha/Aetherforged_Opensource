﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using core;

namespace core.forger
{
    [CreateAssetMenu(fileName = "Archetype", menuName = "Units/Archetype")]
    public class Archetype : ScriptableObject
    {
        public readonly float movementSpeedPerHaste = 0.5f;

        [SerializeField]
        UnitStats baseStats;
        [SerializeField]
        UnitStats perLevelStats;
        [SerializeField]
        float basicAttackDamagePerPower;
        [SerializeField]
        float attackSpeedPerHaste;
        [SerializeField]
        float cooldownReductionPerHaste;


        public UnitStats BaseStats
        {
            get 
            {
                return baseStats;
            }
        }

        public UnitStats PerLevelStats
        {
            get
            {
                return perLevelStats;
            }
        }

        public UnitStats RatioStats(float Power, float Haste)
        {
            return UnitStats.Stats("ratios"
                , AutoAttackDamage: Power * basicAttackDamagePerPower
                , AttackSpeed: Haste * attackSpeedPerHaste
                , CooldownReduction: Haste * cooldownReductionPerHaste
                , MovementSpeed: Haste * movementSpeedPerHaste
            );
        }
    }
}