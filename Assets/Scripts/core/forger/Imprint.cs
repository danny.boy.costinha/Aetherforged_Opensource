﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using core;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

namespace core.forger {
    [CreateAssetMenu(fileName = "New Imprint", menuName = "Units/Imprint")]
    public class Imprint : Kit {

        public Archetype archetype;

        public Sprite face;

        public override UnitStats GetBaseStats ()
        {
            PerLevelStats = archetype.PerLevelStats;
            return archetype.BaseStats;
        }

        public Imprint()
        {
            passives = new Passive[1];
            skills = new SkillRecord[]
            {
                new SkillRecord{slot = 1},
                new SkillRecord{slot = 2},
                new SkillRecord{slot = 3},
                new SkillRecord{slot = 4}
            };
        }

        protected override void Initialize(Unit me)
        {
            base.Initialize(me);
            /*  // We shouldn't have to double define abilities. 
            #region passive ability
            Passives.Add(0, characterPassive);
            #endregion

            #region first active
            Skills.Add(1, firstBasicSkill);
            #endregion

            #region second active
            Skills.Add(2, secondBasicSkill);
            #endregion

            #region third active
            Skills.Add(3, thirdBasicSkill);
            #endregion

            Skills.Add(4, ultimateAbility);
            */
        }

        /// <summary>
        /// Scalable stats for this forger imprint
        /// </summary>
        /// <returns>The stats.</returns>
        /// <param name="power">Power.</param>
        /// <param name="haste">Haste.</param>
        public override UnitStats ScalableStats(float power, float haste)
        {
            return archetype.RatioStats(power, haste);
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(Imprint))]
    public class ItemRootEditor : Editor
    {
        private ReorderableList passivesList;
        private ReorderableList skillsList;

        bool editBasic;

        Imprint kit;

        private void OnEnable()
        {
            kit = (Imprint)target;
            passivesList = new ReorderableList(serializedObject,
                serializedObject.FindProperty("passives"),
                true, true, true, true);

            passivesList.drawHeaderCallback = (rect) => 
            {
                EditorGUI.LabelField(rect, "Innate");
            };

            passivesList.drawElementCallback = (rect, index, isActive, isFocused) => 
            {
                var element = passivesList.serializedProperty.GetArrayElementAtIndex(index);
                rect.y += 2;

                float spaceWidth = 5f;
                float dressWidth = 20f;
                float nameWidth = rect.width - dressWidth - spaceWidth;

                if (element.objectReferenceValue != null)
                {
                    EditorGUI.PropertyField(
                        new Rect(rect.x, rect.y, nameWidth, EditorGUIUtility.singleLineHeight),
                        element, GUIContent.none);
                }
                else
                {
                    EditorGUI.PropertyField(
                        new Rect(rect.x, rect.y, nameWidth, EditorGUIUtility.singleLineHeight),
                        element, new GUIContent("Please select Passive"));
                }
            };

            skillsList = new ReorderableList(serializedObject,
                serializedObject.FindProperty("skills"),
                true, true, true, true);

            skillsList.drawHeaderCallback = (rect) => {
                EditorGUI.LabelField(rect, "Actives");
            };

            skillsList.drawElementCallback =
            (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                var element = skillsList.serializedProperty.GetArrayElementAtIndex(index);
                rect.y += 2;

                float spaceWidth = 5f;
                float dressWidth = 20f;
                float nameWidth = rect.width - dressWidth - spaceWidth;

                if (element.FindPropertyRelative("skill").objectReferenceValue != null)
                {
                    EditorGUI.PropertyField(
                        new Rect(rect.x, rect.y, nameWidth, EditorGUIUtility.singleLineHeight),
                        element, GUIContent.none);
                }
                else
                {
                    string tellUserNicely = "Please select ";
                    if (element.FindPropertyRelative("slot").intValue == 4)
                    {
                        tellUserNicely += "Ultimate";
                    }
                    else 
                    {
                        tellUserNicely += "Castable";
                    }
                    EditorGUI.PropertyField(
                        new Rect(rect.x, rect.y, nameWidth, EditorGUIUtility.singleLineHeight),
                        element, new GUIContent(tellUserNicely));
                }
            };


        }

        public override void OnInspectorGUI()
        {
            float faceWidth = 72f;

            serializedObject.Update();

            if (editBasic)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.BeginVertical();
                EditorGUILayout.BeginHorizontal();
                editBasic = EditorGUILayout.Foldout(editBasic, GUIContent.none);// "Name", true);
                EditorGUILayout.LabelField("Space reserved for descriptive name");
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.PropertyField(serializedObject.FindProperty("kitName"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("attackHitTime"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("archetype"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("resourceFrame"), true);
                EditorGUILayout.EndVertical();
                {
                    var temp = (Sprite)EditorGUILayout.ObjectField(serializedObject.FindProperty("face").objectReferenceValue, typeof(Sprite), false, GUILayout.Width(faceWidth), GUILayout.Height(faceWidth));
                    if (kit.face != temp)
                    {
                        kit.face = temp;
                        EditorUtility.SetDirty(kit);
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
            else 
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.BeginVertical();
                EditorGUILayout.BeginHorizontal();
                editBasic = EditorGUILayout.Foldout(editBasic, GUIContent.none);
                EditorGUILayout.LabelField("Space reserved for descriptive name");
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.LabelField("Kit Name", serializedObject.FindProperty("kitName").stringValue);
                EditorGUILayout.LabelField("Attack Hit Time", serializedObject.FindProperty("attackHitTime").floatValue.ToString());
                if (kit.archetype != null)
                {
                    EditorGUILayout.LabelField("Archetype", serializedObject.FindProperty("archetype").objectReferenceValue.name);
                }
                else 
                {
                    var save_color = GUI.color;
                    GUI.color = Color.red;
                    if (GUILayout.Button("!!! Please set Archetype !!!"))
                    {
                        editBasic = true;
                    }
                    GUI.color = save_color;
                }

                if (kit.resourceFrame != null && kit.resourceFrame.MaxAmount > 0)
                {
                    EditorGUILayout.LabelField(string.Format("Resource: {0}  Between [{2} - {1}]", kit.resourceFrame.Name, kit.resourceFrame.MaxAmount, kit.resourceFrame.MinAmount));
                }

                EditorGUILayout.EndVertical();

                GUIContent myFace = GUIContent.none;
                if (kit.face != null)
                {
                    myFace = new GUIContent(kit.face.texture);
                }
                EditorGUILayout.LabelField(myFace, GUILayout.Width(faceWidth), GUILayout.Height(faceWidth));
                EditorGUILayout.EndHorizontal();
            }
            passivesList.DoLayoutList();
            skillsList.DoLayoutList();

            serializedObject.ApplyModifiedProperties();
        }

    }
#endif
}
