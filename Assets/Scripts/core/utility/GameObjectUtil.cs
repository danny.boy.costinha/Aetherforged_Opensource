﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectUtil {

    /// <summary>
    /// Finds the GameObject. If it finds the GameObject it finds a component <T>.
    /// If not, it still won't break all the things. 
    /// </summary>
    /// <returns>The get component.</returns>
    /// <param name="searchString">Search string.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public static T FindGetComponent<T>(string searchString) where T : Component {
        var gob = GameObject.Find(searchString);
        if (gob != null) {
            return gob.GetComponent<T>();
        } else {
            return default(T);
        }
    }
}
