﻿using UnityEngine;
using System.Collections;

namespace core.utility
{
  public class DestroyMe : MonoBehaviour 
  {
    public void DestroyIt()
    {
      Destroy( gameObject );
    }
  }
}