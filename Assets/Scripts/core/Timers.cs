using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using UnityEngine;

using managers;

namespace core {
    public class Timer {
        private Coroutine coroutine;
        private Ability ability;
        public Ability ChannelAbility {
            get { return ability; }
        }
        public Action<Ability,Unit> OnInterrupt {
            get;
            protected set;
        }

        public Func<float> OnTrigger {
            get;
            protected set;
        }
        public Timer() {

        }
        public Timer(Ability ability, Action<Ability,Unit> OnInterrupt) {
            this.ability = ability;
            this.OnInterrupt = OnInterrupt;
        }

        public Timer Start(Func<float> onTrigger, float delay = 0f) {
            this.OnTrigger = onTrigger;
            if (delay == 0f) {
                delay = onTrigger();
            }
            if (delay >= 0) {
                if (GameManager.Ready)
                {
                    var corrie = GameManager.Instance.DoDelay(onTrigger, delay);
                    this.coroutine = GameManager.Instance.StartCoroutine(corrie);
                }
            }
            return this;
        }

        public void Adjust(float toDelay) {
            var corrie = GameManager.Instance.DoDelay(OnTrigger, toDelay);

            GameManager.Instance.StopCoroutine(this.coroutine);
            this.coroutine = GameManager.Instance.StartCoroutine(corrie);

        }

        public void Stop() {
            if (GameManager.Ready)
            {
                GameManager.Instance.StopCoroutine(coroutine);
            }

        }

        public void Stop(Unit caster) {
            if (GameManager.Ready)
            {
                GameManager.Instance.StopCoroutine(coroutine);
            }
            if (OnInterrupt != null)
            {
                OnInterrupt(ability, caster);
            }
        }
    }
}
