﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using core;

namespace core {
	public class Recastable : Castable {
		public float[] recastWindow;

		public Func<Castable, Unit, bool> RecastConditions;
        public Action<Castable, Unit, Castable> OnRecastableCast;
        public Action<Castable, Unit, Castable> OnRecastableResolve;
	}
}