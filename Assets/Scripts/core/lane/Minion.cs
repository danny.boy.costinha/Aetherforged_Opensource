﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using managers;
using core;

namespace core.lane {
	public class Minion : Unit {
        public override UnitKind Kind {
			get { return UnitKind.Minion; }
		}

        public override BountyType Bounty {
            get {
                return BountyType.Local;
            }
        }

        public override BountyType MoneyBountyKind {
            get {
                return BountyType.Runnoff;
            }
        }


		public float aliveTime;
		public float deadTime;

        protected override void Activate () {
            var minion = GetComponent<Minion>();
            unitName = "Hello. My name is Minion";
            //      longest.Enqueue(myNumber);

            Debug.Assert(minion != null);
            /*
            Color teamColor;
            switch (minion.Team) {
            case Team.DownLeft:
                teamColor = Color.blue;
                break;
            case Team.UpRight:
                teamColor = Color.red;
                break;
            default:
                teamColor = Color.grey;
                break;
            }

            Renderer[] renderers = minion.GetComponentsInChildren<Renderer>();
            foreach (var rend in renderers) {
                if (rend.gameObject.name == "StingPlates") {
                    rend.material.color = teamColor;
                    break;
                }
            }
            */
        }

        protected override void Initialize () {
            Level = 1 + Mathf.FloorToInt(managers.GameManager.Instance.CurrentTick / 60f);
            base.Initialize();
        }

        public override void Colourize () {
            
            var bars = transform.Find("MinionCanvas").GetComponentsInChildren<Image>();

            if (modelReady)
            {
                var rend = transform.GetComponentInChildren<Renderer>();

                var colour = managers.UnitManager.Instance.AppropriateColour(Team);


                foreach (var bar in bars)
                {
                    if (bar.name == "HealthBar")
                    {
                        bar.color = colour;
                    }
                }

                if (rend != null)
                {
                    rend.material.SetColor("_EmissionColor", colour);

                    rend.material.color = colour;
                }
            }
        }

	}


}