﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System.Collections.Generic;
using System.Collections;
using System;
using System.IO;
using System.Net;
using System.Linq;

using core;
using core.forger;
using core.lane;
using managers;
using core.http;
using controllers;
using controllers.ai;
using feature.mainmenu.models;

public class AetherForgedNetworkManager : NetworkManager {
    public Uri GameServerCloseEndpoint { get { return new Uri(NetworkStore.BaseUrl + "/api/GameServer/Close"); } } //TODO: GET
    public Uri GameServerKeepAliveEndpoint { get { return new Uri(NetworkStore.BaseUrl + "/api/GameServer/KeepAlive"); } } //TODO: GET
    public Uri GetUserInfoEndpoint { get { return new Uri(NetworkStore.BaseUrl + "/api/User/GetUserInfoFromSession"); } } //TODO: GET


    private static AetherForgedNetworkManager instance;
    public static bool Ready {
        get { return (instance != null); }
    }
    public static AetherForgedNetworkManager Instance {
        get { return instance; }
    }

    private int port;

    public string DisplayName {
        get;
        set;
    }




    public static GameServerModel clientGameServerReference;
    public static int clientTargetPort = -1;
    public static string clientTargetIp;

    readonly float NEXT_PING_DELAY = 0.5f;
    float nextNewPing;

    public static Ping PingOut {
        get;
        private set;
    }

    public int PlayerCount = 0;

    void Start() {
        string net_type = "other";
        string ip = "localhost";
        port = 7777;
        string secretKey = "";

        networkPort = port;

        if(clientTargetPort != -1) {
            port = clientTargetPort;
            ip = clientTargetIp;
            net_type = "client";
        }
        else if (File.Exists(".matchmaking")) {
            var lines = File.ReadAllLines(".matchmaking");
            if(lines.Length > 0)
            {
                net_type = lines[0];
                if (net_type == "client")
                {
                    ip = lines[1];
                    port = int.Parse(lines[2]);
                }
                if (net_type == "server")
                {
                    port = int.Parse(lines[1]);
                    secretKey = lines[2];
                }
            }
        }

        if(net_type == "server") {
            networkAddress = "0.0.0.0";
            networkPort = port;
            StartServer();
        }
        else if(net_type == "client") {
            networkAddress = ip;
            networkPort = port;
            StartClient();
        }
        else if (Input.GetKey(KeyCode.LeftShift)) {
            networkPort = port;
            StartHost();
        }

        {
            // FIXME needs a session id
            /// NOTE we could get this from logging in, which works great if we're approaching from the main screen.
            /// In the meantime what do?
            var httpPostSess = new HttpPost() {
                Endpoint = GetUserInfoEndpoint,
                JsonModel = new SessionModel {
                    StringIdentifier = "blah blah",
                }
            };
            StartCoroutine(httpPostSess.CoSend((result) => {
                // TODO: get stuff out of the POSTRESULT

            }));

        }

        NewPing();
    }

    // Update is called once per frame
    void Update() {
        if (PingOut == null || PingOut.isDone && nextNewPing <= Time.time) {
            nextNewPing = Time.time + NEXT_PING_DELAY;
            NewPing();
        }

        if (!Ready) {
            instance = this;
        }

    }

    private Hashtable playerObjects = new Hashtable();


    public void ConnectClientToServer (string ipAddy) {
        networkAddress = ipAddy;
        networkPort = port;
        StartClient();
    }

    public void HostAMatch () {
        StartHost();
    }

    public override void OnServerReady(NetworkConnection conn) {
        base.OnServerReady(conn);

        playerObjects[conn.connectionId] = GameManager.Instance.SpawnPlayer(conn);

    }

	public override void OnStartServer ()
	{
		base.OnStartServer();

        var secretKey = "";

        if (File.Exists(".matchmaking")) {
            var lines = File.ReadAllLines(".matchmaking");
            if (lines.Length > 0) {
                var net_type = lines [0];
                if (net_type == "server") {
                    port = int.Parse(lines [1]);
                    secretKey = lines [2];
                }
            }
        }

		Debug.Log("Server Started");


        new Timer().Start(() => {
            if (GameManager.Instance.GameOver) {
                // TODO 2017 Apr 30 I could use a coroutine here. 
                // It's unnecessary, but if we actually need the response, webrequests can't be counted on to take trivial time. 
                // In that case a coroutine would be better. 

                var httpPost = new HttpPost() {
                    Endpoint = GameServerCloseEndpoint,
                    JsonModel = new GameServerModel {
                        SecretKey = secretKey,
                    }
                };
                StartCoroutine(httpPost.CoSend((result) => {


                }));


//                new HttpPost() {
//                    Endpoint = GameServerCloseEndpoint,
//                    JsonModel = new GameServerModel {
//                        SecretKey = secretKey,
//                    }
//                }.Send();

                if (numPlayers == 0) {
                    StopServer();



                #if UNITY_EDITOR

                UnityEditor.EditorApplication.isPlaying = false;

                #else

                Application.Quit();

                #endif
                    return 0;
                }
                return 1;
            }
            return 5;
        }, 30);

	}
    
    public override void OnStopHost ()
    {
        base.OnStopHost();
        GameManager.Instance.Reset();
    }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId) {
//        base.OnServerAddPlayer(conn, playerControllerId);
        NetworkServer.AddPlayerForConnection(conn, (GameObject)playerObjects[conn.connectionId], playerControllerId);
        PlayerCount++;
        Debug.Log("Player Connecting, Players connected : " + PlayerCount);
    }

    public override void OnServerRemovePlayer(NetworkConnection conn, PlayerController player) {
        base.OnServerRemovePlayer(conn, player);

        Debug.Log("Player disconnected from the server");

        PlayerCount--;
    }

    public void NewPing () {
        PingOut = new Ping(networkAddress);
    }
}