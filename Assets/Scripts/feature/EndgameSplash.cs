﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using managers;
using controllers;

namespace feature {
    public class EndgameSplash : MonoBehaviour {

        [SerializeField]
        Image endgameSplash;

        [SerializeField]
        Sprite defeatBanner;

        [SerializeField]
        AudioClip defeatMusic;

        [SerializeField]
        Sprite victoryBanner;

        [SerializeField]
        AudioClip victoryMusic;

        bool splashed = false;

    	// Update is called once per frame
    	void Update () {
            if (endgameSplash == null) {
                

                endgameSplash = GetComponent<Image>();

                if (endgameSplash == null) {
                    endgameSplash = GetComponentInChildren<Image>();
                }

                endgameSplash.sprite = null;

                endgameSplash.enabled = false;

            } else if (GameManager.Instance.GameOver && !splashed) {

//                AudioController.Instance.FadeDuration = 0.3f;

                if (GameManager.Instance.Victor == UnitManager.Instance.HomeTeam) {
                    endgameSplash.sprite = victoryBanner;
                    AudioController.Instance.FadeIn(victoryMusic);
                } else {
                    endgameSplash.sprite = defeatBanner;
                    AudioController.Instance.FadeIn(defeatMusic);
                }

                AudioController.Instance.secondarySource.Play();

                endgameSplash.enabled = true;

                // don't need it yet, but a draw banner maybe good for other modes

                splashed = true;
            }
    	}
    }
}