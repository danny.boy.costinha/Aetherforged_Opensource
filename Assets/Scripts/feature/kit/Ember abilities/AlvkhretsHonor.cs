﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
	[CreateAssetMenu(fileName = "Alvkhrets Honor", menuName = "Ability/Ember/Alvkhrets Honor")]
	public class AlvkhretsHonor : Castable
	{
		//Fires a projectile X
		//On hit, Nibra teleports there dealing damage X
		//Checks within range of the target for enemy forgers X
		//Teleports to the closest on, deals damage, checks again
		//Repeats until there are no more unique enemies
		//Teleports to the first target again, dealing an additional amount of damage
		//Note: Nibra is untargetable during this attack.
		[SerializeField] float[] damage;
		[SerializeField] float powerScaling;
		[SerializeField] float range;
		[SerializeField] float speed;
		[SerializeField] float turnDuration;
		[SerializeField] string aHProjectile = "Akvkhrets_Honor_projectile";
		[SerializeField] float radius;
		[SerializeField] float teleTime;
		[SerializeField] BuffDebuff immunityBuff;
		
		private ProjectileController akvkhretsHonorprojectile;
		private Unit closestForger;
		private Unit currentTarget;
		public AlvkhretsHonor()
		{
			IsUltimate = true;
			
			LineTargetCast = (ability, caster, location) =>
			{
				bool thereIsATarget = true;
				float totalDamage = damage[caster.GetAbilityLevelLessOne(ability)] 
					+ powerScaling * caster.Power;

                Vector2 locationTarget = Utils.PointWithinRange(caster.Position, location, range, range);
				
				akvkhretsHonorprojectile = ProjectileManager.Instance.Gimme(aHProjectile, locationTarget, caster, speed
                    , OnHitUnit: (proj, hit_unit) =>
					{
						caster.HaltAndFaceTarget(locationTarget, turnDuration);
						if(hit_unit.Team != caster.Team && hit_unit.Kind == UnitKind.Forger)
						{
							caster.DealDamage(hit_unit, new DamagePacket(DamageActionType.SkillOrAbility, caster)
							{PhysicalDamage = totalDamage});
							proj.BreakMe();
							
							Vector2 telePoint = hit_unit.Position - (hit_unit.Position - caster.Position).normalized * 100;
							caster.Position = telePoint;

							closestForger = null;
							currentTarget = hit_unit;
							
							while(thereIsATarget)
							{
								closestForger = null;
								Unit.ForEachEnemyInRadius(radius, currentTarget.Position, caster.Team, (Unit enemy) =>
								{
									Debug.Log("Current target is : " + currentTarget);
									
									if(enemy.Kind == UnitKind.Forger && !enemy.HasBuff(immunityBuff.WithSource(caster)))
									{
										if(closestForger == null)
										{
											closestForger = enemy;
										}
										else if(currentTarget.GetDistance(enemy) < currentTarget.GetDistance(closestForger))
										{
											closestForger = enemy;
										}
									}
								});

								if(closestForger == null)
								{
									thereIsATarget = false;
									if(currentTarget != hit_unit)
									{
										Vector2 telePointNew = hit_unit.Position - (hit_unit.Position - caster.Position).normalized * 100;
										caster.Position = telePointNew;
										caster.HaltAndFaceTarget(hit_unit.Position, turnDuration);
										caster.DealDamage(hit_unit, new DamagePacket(DamageActionType.SkillOrAbility, caster)
											{PhysicalDamage = totalDamage});

									}
								}
								else
								{
									Vector2 telePointNew = closestForger.Position - (closestForger.Position - caster.Position).normalized * 100;
									caster.Position = telePointNew;
									caster.DealDamage(closestForger, new DamagePacket(DamageActionType.SkillOrAbility, caster)
										{PhysicalDamage = totalDamage});
									closestForger.AddBuff(immunityBuff.WithSource(caster));

									currentTarget = closestForger;
								}
							}
						}
					});
				caster.StartCooldown(ability);
			};
		}
	}
}
