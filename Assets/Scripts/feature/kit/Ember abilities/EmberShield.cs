﻿using System.Collections.Generic;
using UnityEngine;

using controllers;
using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "Ember Shield", menuName = "Ability/Ember/Ember Shield Buff")]
	public class EmberShield : ShieldBuff
	{
		[SerializeField] float[] shield;
		
		public EmberShield()
		{
			Amount = (pair) =>
			{
				float shieldAmount = shield[pair.Source.GetAbilityLevelLessOne(pair.CastableParent)];
				return (shieldAmount);
			};
			Maximum = (pair) =>
			{
				float shieldAmount = shield[pair.Source.GetAbilityLevelLessOne(pair.CastableParent)];
				return (shieldAmount);
			};
		}
	}
}