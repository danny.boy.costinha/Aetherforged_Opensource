﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "FistOfOuros", menuName = "Ability/Warcardinal/FistOfOuros")]
    public class FistOfOuros : Castable
    {
        
        #region fist of ouros variables
        //fist of ouros base variables
        public float[] fistofOurosCooldown = new float[] { 1, 1, 1 };
        public float fistofOurosRange = 375;
        public int fistofOurosWidth = 250;
        //fist of ouros damage variables
        public float[] fistofOurosBaseDamage = new float[] { 1, 1, 1 };
        public float fistofOurosPowerScaling = .5f;
        //fist of ouros cc variables
        public int fistofOurosKnockbackDistance = 150;
        public int fistofOurosKnockbackHeight = 100;
        public float fistofOurosKnockbackDuration = .5f;
        //fist of ouros debuff variables
        public BuffDebuff fistofOurosDebuff;
        public float fistofOurosStunDuration = 1.5f;
        #endregion

        public FistOfOuros()
        {
            Name = "the_fist_of_ouros";
            //IsUltimate = true;
            Cooldowns = fistofOurosCooldown;
            OnNonTargetedCast = (Castable ability, Unit caster) =>
            {
                //need to change this to be a line, rather than a radial thing, but it works for now, I guess...
                float totalDamage = fistofOurosBaseDamage[caster.GetAbilityLevelLessOne(ability)] + (fistofOurosPowerScaling * caster.Power);
                Kit.ForEnemiesInRadius(fistofOurosWidth, caster, (Unit enemy) =>
                {
                    //enemy.Knockback(caster.Position, fistofOurosKnockbackDuration, fistofOurosKnockbackDistance, fistofOurosKnockbackHeight);
                    caster.DealDamage(enemy, new DamagePacket (DamageActionType.SkillOrAbility, caster)
						{ MagicalDamage = totalDamage });
                    enemy.AddBuff(fistofOurosDebuff.WithSource(caster));

                    caster.PlayAnimation("Ability4");
                });
            };
        }
    }
}