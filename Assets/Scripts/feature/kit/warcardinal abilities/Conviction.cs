﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "Conviction", menuName = "Ability/Warcardinal/Conviction")]
    public class Conviction : Passive
    {
        #region conviction variables
        //conviction base variables
        public Unit convictionPreviousUnit = null;
        //conviction debuff variables
        [SerializeField] BuffDebuff convictionDebuff;
        [SerializeField] Status convictionStun;
        public float convictionDebuffDuration = 5;
        public float convictionStunDuration = .65f;
        //conviction debuff immunity variables
        [SerializeField] BuffDebuff convictionImmunityBuff;
        public float convictionImmunityDuration = 8;
        //conviction resource gain variables
        public float convictionResourceGain = 1;
        public float convictionEmpoweredResourceGain = 2;
        #endregion

        #region passive ability
        public Conviction()
        {
            
            Name = "conviction";
            OnBasicAttack = (SourcePair pair, Unit holder, Unit target, DamagePacket packet) =>
            {
                if (target.Kind == UnitKind.Forger || target.Kind == UnitKind.LargeMonster)
                {
                    holder.Resource += convictionEmpoweredResourceGain;

                    // Penitence
                    var data = holder.GetPassiveData(pair);
                    if (!target.HasBuff(convictionImmunityBuff.WithSource(holder)))
                    {
                        if (target != data.Mark && data.Mark != null)
                        {
                            data.Mark.RemoveBuff(convictionDebuff.WithSource(holder));

                        }

                        if (target.StacksOf(convictionDebuff.WithSource(holder)) >= convictionDebuff.MaximumStacks)
                        {
                            target.RemoveBuff(convictionDebuff.WithSource(holder), fullClear:true);
                            target.AddBuff(convictionStun.WithSource(holder));
                            target.AddBuff(convictionDebuff.WithSource(holder));
                        }
                        else if (!target.HasBuff(convictionImmunityBuff.WithSource(holder)))
                        {
                            target.AddBuff(convictionDebuff.WithSource(holder));
                        }
                        data.Mark = target;
                    }
                }
                else
                {
                    holder.Resource += convictionResourceGain;
                }
                return packet;
            };

        }
        #endregion
    }
}