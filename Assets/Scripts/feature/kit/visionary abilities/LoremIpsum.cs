﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "LoremIpsum", menuName = "Ability/Visionary/LoremIpsum")]
    public class LoremIpsum : Castable
    {

        #region lorem ipsum variables
        //lorem ipsum base variables
        public float[] loremIpsumCooldown = new float[] { 14, 13, 12, 11, 10 };
        public float loremIpsumRange = 850;
        public int loremIpsumInitialRadius = 125;
        public int loremIpsumTotalRadius = 250;
        //lorem ipsum projectile variables
        public float loremIpsumMissileSpeed = 1000;
        public string loremIpsumProjectile = "lorem_ipsum";
        public float[] loremIpsumBaseDamage = new float[] { 100, 100, 100, 100, 100 };
        public float loremIpsumPowerScaling = .5f;
        //lorem ipsum CC variables
        public float loremIpsumSilenceDuration = 1.5f;
        //lorem ipsum other variables
        public float loremIpsumVisionDuration = 3;
        public float loremIpsumVisionGrowthDuration = 2;
        #endregion

        public LoremIpsum()
        {
            Name = "lorem_ipsum";
            Cooldowns = loremIpsumCooldown;
            OnNonTargetedCast = (Castable ability, Unit caster) =>
            {
                float baseDamage = loremIpsumBaseDamage[ability.Level - 1];
                float totalDamage = baseDamage + (loremIpsumPowerScaling * caster.Power);
                //throw projectile, on landing deal damage in AoE around it and start granting vision which expands over time to max range

                Vector2 locationTarget;
                if (Utils.TryGetPlanarPointWithinRangeFromMousePosition(out locationTarget, caster.Position, loremIpsumRange))
                {
                    caster.PlayAnimation("Ability3", locationTarget);
                    ProjectileManager.Instance.Gimme(loremIpsumProjectile, locationTarget, caster, loremIpsumMissileSpeed
                        , OnStop: (proj) =>
                        {
                            Unit.ForEachEnemyInRadius(loremIpsumInitialRadius, locationTarget, caster.Team, (Unit target) =>
                            {
                                caster.DealDamage(target, new DamagePacket(DamageActionType.SkillOrAbility, caster) { MagicalDamage = totalDamage });
                                    //add silence to unit
                                });
                                //create ground object that has an expanding vision radius
                                proj.BreakMe();
                        }
                    );

                }
                caster.StartCooldown(ability);

            /*var testLineAbility = new Castable()
            {
                Name = "test_line",
                Cooldowns = new float[] {1, 1, 1, 1, 1},
                OnLineTargetCast = (Castable ability, Unit caster) => {

                    Vector2 orig = new Vector2();
                    orig = Utils.PlanarPoint(caster.gameObject.transform.position);

                    Vector2 target = new Vector2();
                    if(Utils.TryGetPlanarPointFromMousePosition(out target))
                    {
                        Utils.GetUnitsInLine(orig, target, 150, 1000);
                    }
                },
            };*/ 

            };
        }
    }
}