﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using managers;
using core.forger;
using core.utility;
using core;

namespace feature.kit
{
    [CreateAssetMenu(fileName = "Examine", menuName = "Ability/Visionary/Examine")]
    public class Examine : Passive
    {
        #region examine variables
        //examine timer variables
        public Timer examineChaseDelay;
        public int examineChasePrime = 10;
        //examine ward variables
        public float examineChaseDuration = 3.5f;
        public int examineChaseRadius = 200;
        public float examineWardCooldownReductionPercentage = .5f;
        #endregion

        public Examine() {

            Name = "examine";
            //alter the way that Sil's ward works when the game beings

        }
    }
}