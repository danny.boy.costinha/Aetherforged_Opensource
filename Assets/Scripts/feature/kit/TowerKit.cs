﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using core;
using core.forger;

namespace feature.kit {
    [CreateAssetMenu(fileName = "Tower Kit", menuName = "Units/Tower Kit")]
    public class TowerKit : Kit {

    	Unit repeatTarget;

        Transform attachPoint;
        /*
    	public override string ObsoleteName {
    		get {
    			return "Tower";
    		}
    	}
        */
        public override string AutoAttackProjectile
        {
            get
            {
                return "tower projectile";
            }
        }


        protected override void Initialize (Unit myUnit)
        {
            /*
            
            {
                int index = 0;
                foreach (Passive ability in passives)
                {
                    Passives.Add(index++, ability);
                }
            }

            */


            base.Initialize(myUnit);
    	}




    	public override UnitStats GetBaseStats ()
    	{
                PerLevelStats = UnitStats.Stats(
                    "Tiers" 
                    , AutoAttackDamage: 100   // Attack damage for Tier 3 should be 250
                    , MaxHealth: 1500
                );

    		return UnitStats.Stats(
                    "BaseTower"
                    , AutoAttackDamage: 100
                    , MaxHealth: 3000
                    , Armor: 50
                    , Resistance: 0
                    , BaseAttackRange: 575
                    , BaseAttackTime: 2.0f
                    /*, AttackHitTime: 0.3f*/
                    , Hephisalt: 150f
                    , Experience: 200f
    		);
    	}
        /*
    	public override ForgerResource characterResource ()
    	{
    		return new ForgerResource();
    	}
        */
    }
}