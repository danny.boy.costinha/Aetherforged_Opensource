﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowedMode : MonoBehaviour {
	void Update () {
		if(Screen.fullScreen) {
            Screen.fullScreen = false;
        }
	}

    public void ExitApp() {
        Application.Quit();
    }
}
