﻿using core;
using core.forger;

namespace feature.kit
{
    public class AbilityBruiser : Obsolete_Archetype
    {
        public override UnitStats BaseStats
        {
            get
            {
                return UnitStats.Stats(
                    MaxHealth: 515,
                    HealthRegen: 1.5f,
                    MovementSpeed: 390,
                    AutoAttackDamage: 59,
                    BaseAttackTime: 1.5f,
                    BaseAttackRange: Constants.MELEE_DEFAULT_ATTACK_RANGE
                );
            }
        }

        public override UnitStats PerLevelStats
        {
            get
            {
                // Note: Right now (level - 1) is applied from per level stats. 
                // E.g. At level one, they get BaseStats only. At level two, they get BaseStats + 1 x PerLevelStats. 
                // And At level six, they get BaseStats + 5 x PerLevelStats.

                return UnitStats.Stats(
                    MaxHealth: 65,
                    HealthRegen: 1.5f,
                    AutoAttackDamage: 2.4f,
                    AttackSpeed: 3.10f
                );
            }
        }

        public override float basicAttackDamagePerPower
        {
            get
            {
                return .9f;
            }
        }

        public override float attackSpeedPerHaste
        {
            get
            {
                return .6f;
            }
        }

        public override float cooldownReductionPerHaste
        {
            get
            {
                return .6f;
            }
        }

    }
}