using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature {
    public enum StatType {
        MaxHealth,
        HealthRegen,
        Armor,
        Resistance,
        Power,
        Penetration,
        Mastery,
        Haste,
        PercentMovementSpeed,
        Lifedrain
    }

    public class ItemLoader : Singleton<ItemLoader>
    {
        public ItemRoots firstTier;

        public ItemLoader()
        {

        }

        #region Life Tree
        #region Tier I
        /*
        public float[] LifePassiveCooldown = new float[] { 20 };
        public float LifePassiveRegen = 15;
        public float LifePassiveDuration = 5;
        public static Passive LifePassive;
        */
        #endregion
        #region Tier II
        public float[] LifePlusPassiveCooldown = new float[] { 20 };
        public float LifePlusPassiveRegen = 15;
        public float LifePlusPassiveDuration = 5;
        public static Passive LifePlusPassive;

        public float VigorPassiveSpeedBuffValue = 15;
        public float VigorPassiveSpeedBuffDuration = 1.5f;
        public static Passive VigorPassive;
        public static BuffDebuff VigorPassiveSpeedBuff;

        public float[] SympathyPassiveCooldown = new float[] { 15 };
        public float SympathyPassiveHeal = 25;
        public float SympathyPassiveRange = 400;
        public float SympathyMissleSpeed = 1200;
        public string SympathyProjectileName = "Sympathy Heal";
        public static Passive SympathyPassive;

        public float GrowthPassiveHeal = 250;
        public float GrowthPassiveDuration = 10;
        public static Passive GrowthPassive;
        #endregion
        #region Tier III
        #region Vitality Tree
        public float[] DefiancePassiveCooldown = new float[] { 80 };
        public float DefiancePassiveHealPercentage = .3f;
        public float DefiancePassiveHealFloorPercentage = .3f;
        public float DefiancePassiveHealDuration = 5;
        public static Passive DefiancePassive;

        public float RenewalPassiveBaseEffectiveness = 15;
        public float RenewalPassiveMaxEffectiveness = 50;
        public float RenewalPassiveBuildupDuration = 4;
        public float RenewalPassiveOutofCombatDuration = 3;
        public static Passive RenewalPassive;

        public float RampancyPassivePercentage = .025f;
        public static Passive RampancyPassive;
        #endregion
        #region Vigor Tree
        public float TenacityPassiveTenacityPercentage = 35;
        public static Passive TenacityPassive;

        public float[] EndurancePassiveCooldown = new float[] { 30 };
        public static Passive EndurancePassive;
        #endregion
        #region Sympathy Tree
        public float[] SanctuaryPassiveCooldown = new float[] { 10 };
        public float SanctuaryPassiveAuraBaseArmor = 20;
        public float SanctuaryPassiveAuraBaseMR = 20;
        public float SanctuaryPassiveAuraRange = 400;
        public float SanctuaryPassiveHealBase = 35;
        public float SanctuaryPassiveHealPowerBonus = .1f;
        public float SanctuaryBuffBonusArmor = 20;
        public float SanctuaryBuffBonusMR = 20;
        public float SanctuaryBuffBonusDuration = 1.5f;
        public static Passive SanctuaryPassive;
        public static BuffDebuff SanctuaryBuff;

        public float EmpathyBuffPercentReduction = .15f;
        public float EmpathyBuffPercentReduced = -15;
        public float EmpathyBuffShutdownPercentage = .25f;
        public float EmpathyPassiveMaxRange = 400;
        public static Passive EmpathyPassive;
        public static BuffDebuff EmpathyBuff;

        public float CharityPassivePercentIncrease = .01f;
        public float CharityPassivePerMissingHealthPercent = .02f;
        public static Passive CharityPassive;

        public float SynergyDebuffBaseDamage = 85;
        public float SynergyDebuffDamagePowerBonus = .3f;
        public float SynergyDebuffDamageDuration = 3;
        public float SynergyBaseShield = 50;
        public float SynergyShieldPowerBonus = .2f;
        public float SynergyShieldDuration = 3;
        public float SynergyPassiveAllyLockoutDuration = 10;
        public Unit SynergyPassiveMarkedEnemy = null;
        public static Passive SynergyPassive;
        public static BuffDebuff SynergyDebuffMark;
        public static BuffDebuff SynergyLockout;
        public static ShieldBuff SynergyShield;

        #endregion
        #region Growth Tree
        public float[] MaturityPassiveCooldown = new float[] { 75 };
        public float MaturityPassiveHealthPerStack = 30;
        public float MaturityPassiveArmorPerStack = 3;
        public float MaturityPassiveMRPerStack = 2;
        public float MaturityPassiveTickDuration = 75;
        public int MaturityPassiveMaxStacks = 11;
        public static Passive MaturityPassive;
        public static BuffDebuff MaturityBuff;

        public float ProliferationPassiveBonusDamage = 10;
        public float ProliferationPassiveMaxBonusDamage = 90;
        public float ProliferationPassiveRadius = 550;
        public static Passive ProliferationPassive;
        #endregion
        #endregion
        #endregion

        #region Toughness Tree
        #region Tier I
        /*
        public float ToughnessPassiveDamageBlock = 3;
        public float ToughnessPassiveDamageBlockForgers = 6;
        public float ToughnessPassiveEffectivenessAfterFirst = .5f;
        public static Passive ToughnessPassive;
        */
        #endregion
        #region Tier II
        public float IntegrityPassiveReductionPercentage = -10;
        public static Passive IntegrityPassive;

        public float SubstancePassiveBonusArmorPercentage = .1f;
        public float SubstancePassiveEffectReduction = .5f;
        public static Passive SubstancePassive;

        public float RestraintDebuffPowerReduction = -10;
        public float RestraintDebuffDuration = 2;
        public static Passive RestraintPassive;
        public static BuffDebuff RestraintDebuff;

        public float[] ProtectionPassiveCooldown = new float[] { 10 };
        public float ProtectionBuffArmorPercentage = .15f;
        public float ProtectionBuffMagicResistPercentage = .15f;
        public float ProtectionBuffDuration = 2;
        public bool ProtectionIsPhysicalDamage = true;
        public static Passive ProtectionPassive;
        public static BuffDebuff ProtectionBuff;
        #endregion
        #region Tier III
        #region Law Passive
        public float LawPassiveBaseReduction = -20;
        public float LawPassiveIncreasedReduction = -30;
        public float LawSlowPercentage = .7f;
        public float LawSlowDuration = .5f;
        public float LawLockoutDuration = 5;
        public static Passive LawPassive;
        public static BuffDebuff LawLockout;
        public static Status LawSlow;
        #endregion
        #region Substance Tree
        public float GravityLockdownStackDuration = 4;
        public int GravityLockdownMaxStacks = 5;
        public float GravitySlowPercentage = .35f;
        public float GravitySlowDuration = 1;
        public float GravitySlowRange = 225;
        public static Passive GravityPassive;
        public static BuffDebuff GravityLockdown;
        public static Status GravitySlow;

        public float PridePassiveCooldown = 30 ;
        public float PrideBuffAbilityDamageReduction = -10;
        public float PrideOutofCombatDuration = 6;
        public int PrideBuffMaxStacks = 2;
        public float PrideBuffDuration = 4;
        public static Passive PridePassive;
        public static BuffDebuff PrideBuff;
        public static BuffDebuff PrideOutofCombatTimer;
        public static BuffDebuff PrideLockout;
        #endregion
        #region Restraint Tree
        public float SubjugationDebuffPowerReduction = -15;
        public float SubjugationDebuffAttackSpeedReduction = -15;
        public float SubjugationDebuffDuration = 2;
        public int SubjugationDebuffMaxStacks = 2;
        public static Passive SubjugationPassive;
        public static BuffDebuff SubjugationDebuff;

        public float SuppressionDebuffReduction = -20f;
        public float SuppressionDebuffDuration = 2;
        public static Passive SuppressionPassive;
        public static BuffDebuff SuppressionDebuff;
        #endregion
        #region Protection Tree
        public float[] HopePassiveCooldown = new float[] { 15 };
        public float HopePassivePercentHealthTrigger = .3f;
        public float HopeShieldPercentReduction = -20;
        public float HopeShieldBaseAmount = 100;
        public float HopeShieldPerLevel = 20;
        public float HopeShieldDuration = 3;
        public static Passive HopePassive;
        public static ShieldBuff HopeShield;

        public float AdaptationPassiveBonusArmorBase = 20;
        public float AdaptationPassiveBonusMRBase = 20;
        public float AdaptationPassiveMaxBonusArmor = 40;
        public float AdaptationPassiveMaxBonusMR = 40;
        public float AdaptationPassiveArmorIncrement = 5;
        public float AdaptationPassiveMRIncrement = 5;
        public int AdaptationArmorBuffMaxStacks = 4;
        public int AdaptationMRBuffMaxStacks = 4;
        public static Passive AdaptationPassive;
        public static BuffDebuff AdaptationArmorBuff;
        public static BuffDebuff AdaptationMRBuff;
        #endregion
        #endregion
        #endregion

        #region Resistance Tree
        #region Tier I
        /*
        public float[] ResistancePassiveCooldown = new float[] { 5 };
        public float ResistancePassiveDamageBlock = 15;
        public float ResistancePassiveEffectivenessAfterFirst = .5f;
        public static Passive ResistancePassive;
        */
        #endregion
        #region Tier II
        public float[] PerseverancePassiveCooldown = new float[] { 45 };
        public float PerseverancePassiveHealthTriggerPercentage = .3f;
        public float PerseveranceShieldValue = 200;
        public float PerseverancePassiveReductionPercentage = .5f;
        public float PerseveranceShieldDuration = 6;
        public static Passive PerseverancePassive;
        public static ShieldBuff PerseveranceShield;

        public float ConfidenceDebuffMRReduction = -3;
        public float ConfidenceDebuffDuration = 5;
        public int ConfidenceDebuffMaxStacks = 4;
        public static Passive ConfidencePassive;
        public static BuffDebuff ConfidenceDebuff;
        #endregion
        #region Tier III
        #region Perseverance Tree
        public int ResolvePassiveMaxPower = 35;
        public float ResolvePassiveMaxPowerHealthFloor = .3f;
        public float ResolvePassiveMissingHealthRatio = .5f;
        public static Passive ResolvePassive;
        public static BuffDebuff ResolveBuff;

        public float RetributionPassiveHealthTrigger = .3f;
        public static Passive RetributionPassive;
        public static BuffDebuff RetributionLockout;

        public float FocusShieldBaseValue = 90;
        public float FocusShieldPerLevel = 13;
        public float FocusLockoutDuration = 10;
        public static Passive FocusPassive;
        public static ShieldBuff FocusShield;
        public static BuffDebuff FocusLockout;
        #endregion
        #region Confidence Tree
        public float ArroganceMRShredPerStack = -5;
        public float ArroganceMRBuffPerStack = 5;
        public int ArroganceMRShredMaxStacks = 3;
        public int ArroganceMRBuffMaxStacks = 15;
        public float ArroganceShredDuration = 5;
        public static Passive ArrogancePassive;
        public static BuffDebuff ArroganceMRShred;
        public static BuffDebuff ArroganceMRBuff;

        public float CourageBonusDamage = 50;
        public static Passive CouragePassive;
        #endregion
        #endregion
        #endregion

        #region Power Tree
        #region Tier II
        public float[] AggressionPassiveCooldown = new float[] { 10 };
        public float AggressionPassiveBaseDamage = 15;
        public float AggressionPassivePowerScaling = .4f;
        public static Passive AggressionPassive;

        public float PotentialBuffHealthPerStack = 4;
        public float PotentialBuffPowerPerStack = .5f;
        public int PotentialBuffMaxStacks = 40;
        public static Passive PotentialPassive;
        public static BuffDebuff PotentialBuff;
        public static BuffDebuff PotentialFullBuff;
        #endregion
        #region Tier III
        #region Authority Tree
        public float RebellionPassiveDefensePen = 30;
        public static Passive RebellionPassive;

        public float DominancePassivePercentBonusStatsPen = .5f;
        public static Passive DominancePassive;

        public float JusticeArmorDebuffPerStack = -5;
        public int JusticeArmorDebuffMaxStacks = 5;
        public float JusticeArmorDebuffDuration = 4;
        public float JusticeMRDebuffPerStack = -5;
        public int JusticeMRDebuffMaxStacks = 5;
        public float JusticeMRDebuffDuration = 4;
        public static Passive JusticePassive;
        public static BuffDebuff JusticeArmorDebuff;
        public static BuffDebuff JusticeMRDebuff;
        #endregion
        #region Intensity Tree
        public float PercisionBuffDamagePercent = .25f;
        public int PercisionBuffMaxStacks = 4;
        public float PercisionDummyDuration = 8;
        public float PercisionBuffDamageRange = 400;
        public static Passive PercisionPassive;
        public static BuffDebuff PercisionBuff;
        public static BuffDebuff PercisionDummyBuff;

        public float PainDamagePowerScaling = 1.5f;
        public float PainDamageDuration = 2;
        public static Passive PainPassive;

        public float ForcePassiveDamageRatio = .3f;
        public static Passive ForcePassive;

        public float CommandBuffPower = 10;
        public float CommandBuffMastery = 5;
        public float CommandBuffDuration = 6;
        public int CommandBuffMaxStacks = 3;
        public float CommandPassiveRange = 450;
        public static Passive CommandPassive;
        public static BuffDebuff CommandBuff;

        public float DestructionImmunityDuration = 6;
        public float DestructionPassiveBaseDamage = 30;
        public float DestructionPassiveBonusPower = .4f;
        public float DestructionPassiveBasePercentDamage = .03f;
        public float DestructionPassiveBonusPowerPercentDamage = .01f;
        public static Passive DestructionPassive;
        public static BuffDebuff DestructionImmunity;
        #endregion
        #region Aggression Tree
        public float FatigueDebuffIncreasedDamage = 10;
        public float FatigueDebuffDuration = 4;
        public static Passive FatiguePassive;
        public static BuffDebuff FatigueDebuff;

        public float SupremacyBuffPercentPower = 30;
        public float SupremacyBuffDuration = 4;
        public float[] SupremacyPassiveCooldown = new float[] { 6 };
        public static Passive SupremacyPassive;
        public static BuffDebuff SupremacyBuff;

        public float[] WrathPassiveCooldown = new float[] { 2 };
        public float WrathBuffBaseDamage = 50;
        public float WrathBuffPowerScaling = 1.2f;
        public float WrathBuffDuration = 6;
        public static Passive WrathPassive;
        public static BuffDebuff WrathBuff;

        public float DesolationImmunityCooldown = 6;
        public float DesolationDebuffDuration = 3.5f;
        public float DesolationPassiveBaseDamage = 30;
        public float DesolationPassivePowerScaling = .4f;
        public static Passive DesolationPassive;
        public static BuffDebuff DesolationImmunity;
        #endregion
        #region Potential Tree
        public int DivinityBuffStacksToReduction = 40;
        public float DivinityBuffPercentReduction = 5f;
        public static Passive DivinityPassive;
        public static BuffDebuff DivinityBuff;

        public int ConquestStatBuffMaxStacks = 40;
        public int ConquestDamageBuffMaxStacks = 8;
        public float ConquestDamageBuffTickDuration = 10;
        public float ConquestDamageBuffBaseDamage = 30;
        public float ConquestDamageBuffPowerScalingPerStack = .15f;
        public static Passive ConquestPassive;
        public static BuffDebuff ConquestStatBuff;
        public static BuffDebuff ConquestDamageBuff;
        public static BuffDebuff ConquestStackBuildTimer;
        #endregion
        #endregion
        #endregion

        #region Time Tree
        #region Tier II
        public float DecayPassiveDamage = 20;
        public static Passive DecayPassive;

        public int MaxRecurrenceBuffStacks = 100;
        public float RecurrenceBuffStacksBuiltPerCast = 10;
        public float RecurrenceBuffStacksBuiltPerStep = 2;
        public float RecurrenceHasteBuffAmount = 15;
        public float RecurrenceHasteBuffDuration = 3;
        public float[] RecurrenceCooldown = new float[] { 3 };
        public static Passive RecurrencePassive;
        public static BuffDebuff RecurrenceBuff;
        public static BuffDebuff RecurrenceHasteBuff;

        public float ZealImmunityDuration = 16;
        public float ZealDebuffDuration = 4;
        public float ZealBuffSpeedBoost = 20;
        public float ZealBuffDuration = 2;
        public static Passive ZealPassive;
        public static BuffDebuff ZealDebuff;
        public static BuffDebuff ZealBuff;
        public static BuffDebuff ZealImmunityDebuff;

        public float FateImmunityDuration = 4;
        public float FateSlowDuration = 1;
        public float FateSlowPercentage = -0.15f;
        public static Passive FatePassive;
        public static BuffDebuff FateImmunity;
        public static Status FateSlow;
        #endregion
        #region Tier III
        #region Decay Tree
        public float RuinPassiveBaseDamage = 25;
        public float RuinPassiveTargetBonusHealthDamage = .12f;
        public static Passive RuinPassive;

        public int TurmoilPassiveExtraTargets = 3;
        public float TurmoilPassiveTargetRange = 250;
        public float TurmoilPassivePercentageDamage = .4f;
        public static Passive TurmoilPassive;

        public float MomentumPassiveBasePercentage = .05f;
        public float MomentumRangeBuffPercentage = 20;
        public float MomentumDamageBuffBonusPercentage = .1f;
        public float MomentumBuffLockoutDuration = 8;
        public static Passive MomentumPassive;
        public static BuffDebuff MomentumBuff;
        public static BuffDebuff MomentumBuffLockout;

        public float DissonancePassiveExtraAttacks = 1;
        public float DissonancePassiveNewCritDamageMod = -90;
        public static Passive DissonancePassive;
        #endregion
        #region Recurrence Tree
        public float FuryPassiveMaxHealthPercent = .05f;
        public float FuryPassiveDamageDuration = 3;
        public static Passive FuryPassive;

        public float FinesseAreaOfEffectDelay = .75f;
        public float FinesseImmunityDuration = 1;
        public float FinesseAreaOfEffectBaseDamage = 60;
        public float FinesseAreaOfEffectBasePowerRatio = .4f;
        public float FinesseAreaOfEffectBonusDamagePerStack = 15;
        public float FinesseAreaofEffectBonusPowerRatioPerStack = .1f;
        public float FinesseAreaofEffectRadius = 225;
        public int FinesseBuffMaxStacks = 4;
        public float[] FinessePassiveCooldown = new float[] { 6 };
        public static Passive FinessePassive;
        public static BuffDebuff FinesseBuff;
        public static BuffDebuff FinesseImmunity;
        public static BuffDebuff FinesseBuildingPower;

        public float ResonanceLockoutDuration = 1;
        public float ResonancePassiveCooldownReduced = .25f;
        public float ResonancePassiveRange = 500;
        public static Passive ResonancePassive;
        public static BuffDebuff ResonanceAllyBuff;
        public static BuffDebuff ResonanceLockout;

        public float ContinuityPassiveBaseCooldownReduction = .25f;
        public float ContinuityPassiveForgerCooldownReduction = .5f;
        public float[] ContinuityPassiveCooldown = new float[] { .5f };
        public static Passive ContinuityPassive;
        #endregion
        #region Zeal Tree
        public float AllureBuffBaseMovementSpeed = 10;
        public float AllureBuffOutofCombatMovementSpeed = 20;
        public float AllurePassiveAuraRange = 800;
        public static Passive AllurePassive;
        public static BuffDebuff AllureBuff;

        public int InfluenceBuffBuildingMaxStacks = 3;
        public float InfluenceSpeedBuffIncrease = 30;
        public float InfluenceSpeedBuffDuration = 2;
        public float InfluencePassiveBonusAttackDamage = .5f;
        public float InfluenceMarkDuration = 6;
        public static Passive InfluencePassive;
        public static BuffDebuff InfluenceSpeedBuff;
        public static BuffDebuff InfluenceBuffBuilding;
        public static BuffDebuff InfluenceBuff;
        public static BuffDebuff InfluenceMark;

        public float CorruptionImmunityDuration = 5;
        public float CorruptionDebuffDuration = 3;
        public float CorruptionSpeedBuffDuration = 2;
        public float CorruptionSpeedBuffIncrease = 20;
        public static Passive CorruptionPassive;
        public static BuffDebuff CorruptionSpeedBuff;
        public static BuffDebuff CorruptionDebuff;
        public static BuffDebuff CorruptionImmunity;
        public static BuffDebuff CorruptionLockout;
        #endregion
        #region Fate Tree
        public int EntropyBuffMaxStacs = 4;
        public float EntropyBuffChargeCooldown = 5;
        public float EntropySpeedBuffAmount = 60;
        public float EntropySpeedBuffDuration = .75f;
        public float EntropySlowAmount = 60;
        public float EntropySlowDuration = .75f;
        public static Passive EntropyPassive;
        public static BuffDebuff EntropyBuff;
        public static BuffDebuff EntropyBuffTick;
        //TODO: This speed buff might need to be a status instead?
        public static BuffDebuff EntropySpeedBuff;
        public static Status EntropySlow;

        public float ProphecyImmunityDuration = 5;
        public float ProphecyRangedSlowPercent = .25f;
        public float ProphecyMeleeSlowPercent = .4f;
        public float ProphecySingleTargetSlowPercent = .35f;
        public float ProphecyAoESlowPercent = .15f;
        public float ProphecyMaxSlowPercent = .7f;
        public float ProphecyBuildingSlowDuration = .75f;
        public float ProphecyFinalSlowDuration = .25f;
        public static Passive ProphecyPassive;
        public static BuffDebuff ProphecyImmunity;
        public static Status ProphecyRangedSlow;
        public static Status ProphecyMeleeSlow;
        public static Status ProphecySingleTargetSlow;
        public static Status ProphecyAoESlow;
        //TODO: May not be needed, depending on how increasing/decaying slows work
        public static Status ProphecyFinalSlow;

        public float InsightImmunityDuration = 10;
        public int InsightDebuffStackPerRanged = 1;
        public int InsightDebuffStackPerMelee = 2;
        public float InsightDebuffDuration = 4;
        public int InsightDebuffMaxStacks = 5;
        public float InsightRootDuration = 1;
        public static Passive InsightPassive;
        public static BuffDebuff InsightImmunity;
        public static BuffDebuff InsightDebuff;
        public static BuffDebuff InsightDebuffTimer;
        public static Status InsightRoot;
        #endregion
        #endregion
        #endregion

        #region Hunger Tree
        #region Tier I
        /*
        public float[] HungerPassiveCooldown = new float[] { 1.5f };
        public float HungerPassiveHealed = 5;
        public float HungerPassiveHealedForgers = 10;
        public static Passive HungerPassive;
        */
        #endregion
        #region Tier II
        public float DesireBuffDuration = 3;
        public float DesireBuffDamagePerStack = 5;
        public float DesireBuffHealPercent = .3f;
        public int DesireBuffMaxStacks = 3;
        public float DesirePassiveRangedHealPenalty = .5f;
        public static Passive DesirePassive;
        public static BuffDebuff DesireBuff;
        #endregion
        #region Tier III
        #region Consumption Tree
        public float VoracityBuffLifeDrainPerStack = 3;
        public int VoracityBuffMaxStacks = 5;
        public float VoracityBuffDuration = 4;
        public int VoracityPassiveStacksForger = 2;
        public static Passive VoracityPassive;
        public static BuffDebuff VoracityBuff;

        public float GluttonyPassiveHealPercent = .15f;
        public float GluttonyPassiveAoEHealPercent = .5f;
        public static Passive GluttonyPassive;
        #endregion
        #region Desire Tree
        public float[] AvaricePassiveCooldown = new float[] { .5f };
        public float AvaricePassiveRange = 170;
        public float AvaricePassiveHealthDamageRatio = .75f;
        public float AvaricePassiveHealthHealRatio = .05f;
        public float AvaricePassiveHealthHealRatioForgers = .1f;
        public float AvaricePassiveMaxHealRatio = .375f;
        public static Passive AvaricePassive;

        public float EnvyPassiveBaseDamage = 40;
        public float EnvyPassivePowerScaling = .25f;
        public float EnvyPassiveSplashRange = 200;
        public float EnvyPassiveHealPercentage = .125f;
        public static Passive EnvyPassive;

        public float LustBuffDuration = 3;
        public float LustPassiveDamagePercentage = -15;
        public float LustBuffHealIncrease = 25f;
        public static Passive LustPassive;
        public static BuffDebuff LustBuff;
        #endregion
        #endregion
        #endregion

        private Dictionary<string, Item> Items;

        public string Name { get { return "Items";}}

        public Dictionary<string, Item> InitItems() {

            Items = new Dictionary<string, Item>();
            Item[] temp = new Item[] {
                new Item () {
                    Name = "Life",
                },
                new Item () {
                    Name = "Toughness",
                },
                new Item () {
                    Name = "Resistance",
                    
                    Cost = 300,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Resistance: 15);
                    },
                    ListStats = new List<StatType>() { StatType.Resistance },
                    Parent = null,
                    Child = new string[] {"Protection", "Perseverance", "Confidence"},
                    PassiveName = "Resistance passive",
                    PassiveDesc = "Stackable: When you take magic damage, reduce it by 15. This has a cooldown of 5 seconds.",
                    //StackablePassive = ResistancePassive
                },
                new Item() {
                    Name = "Power",
                },
                new Item() {
                    Name = "Time",
                },
                new Item() {
                    Name = "Hunger",
                },
                new Item() {
                    Name = "Vitality",
                },
                new Item() {
                    Name = "Vigor",
                },
                new Item() {
                    Name = "Sympathy",
                },
                new Item() {
                    Name = "Growth",
                },
                new Item() {
                    Name = "Integrity",
                },
                new Item() {
                    Name = "Substance",
                },
                new Item() {
                    Name = "Restraint",
                },
                new Item() {
                    Name = "Protection",
                },
                new Item() {
                    Name = "Perseverance",
                },
                new Item() {
                    Name = "Confidence",
                    Tier = 2,
                    Cost = 1500,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Resistance: 35, Power: 20);
                    },
                    ListStats = new List<StatType>() { StatType.Resistance, StatType.Power },
                    Parent = new string[] {"Resistance"},
                    Child = new string[] {"Arrogance", "Courage", "Doubt"},
                    PassiveName = "ConfidencePassive",
                    PassiveDesc = "Stackable: Whenever you deal damage to an enemy unit, reduce their Magic Resistance by 3 for 5 seconds. This stacks up to 4 times.",
                    StackablePassive = ConfidencePassive
                },
                new Item() {
                    Name = "Authority",
                    Tier = 2,
                    Cost = 1500,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 30, Penetration: 15);
                    },
                    ListStats = new List<StatType>() {StatType.Power, StatType.Penetration},
                    Parent = new string[] {"Power"},
                    Child = new string[] {"Rebellion", "Dominance", "Justice"},
                    PassiveName = "",
                    PassiveDesc = ""
                },
                new Item() {
                    Name = "Intensity",
                    Tier = 2,
                    Cost = 1500,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 30, Mastery: 10);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Mastery },
                    Parent = new string[] {"Power"},
                    Child = new string[] {"Precision", "Pain", "Force", "Command", "Destruction"},
                    PassiveName = "",
                    PassiveDesc = ""
                },
                new Item() {
                    Name = "Aggression",
                    Tier = 2,
                    Cost = 1200,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 30);
                    },
                    ListStats = new List<StatType>() { StatType.Power },
                    Parent = new string[] {"Power"},
                    Child = new string[] {"Fatigue", "Supremacy", "Wrath", "Desolation"},
                    PassiveName = "AggressionPassive",
                    PassiveDesc = "Stackable: Dealing damage to an enemy causes them to take an additional 15 (+40% Power) damage. This effect has a 10 second cooldown.",
                    StackablePassive = AggressionPassive
                },
                new Item() {
                    Name = "Potential",
                    Tier = 2,
                    Cost = 900,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 140, Power: 15);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Power },
                    Parent = new string[] {"Power"},
                    Child = new string[] {"Divinity", "Conquest"},
                    PassiveName = "PotentialPassive",
                    PassiveDesc = "UNIQUE: When you kill a minion, monster, or miner you gain 4 bonus Health and .5 bonus Power permanently. This stacks up to 40 times.",
                    StackablePassive = PotentialPassive
                },
                new Item() {
                    Name = "Decay",
                    Tier = 2,
                    Cost = 1500,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 15, Haste: 25);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Haste },
                    Parent = new string[] {"Time"},
                    Child = new string[] {"Ruin", "Turmoil", "Momentum", "Dissonance"},
                    PassiveName = "DecayPassive",
                    PassiveDesc = "Stackable: Your basic attacks deal an additional 20 bonus physical damage.",
                    StackablePassive = DecayPassive
                },
                new Item() {
                    Name = "Recurrence",
                    Tier = 2,
                    Cost = 1500,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 10, Haste: 30);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Haste },
                    Parent = new string[] {"Time"},
                    Child = new string[] {"Fury", "Finesse", "Resonance", "Continuity"},
                    PassiveName = "RecurrencePassive",
                    PassiveDesc = "Stackable: Whenever you cast a spell or move, you build stacks of {thing}, up to 100. At max stacks, the next time you damage an enemy with an ability, the stacks are consumed and you gain 15 haste for 3 seconds. You cannot start building stacks of {thing} again for 3 seconds.",
                    StackablePassive = RecurrencePassive
                },
                new Item() {
                    Name = "Zeal",
                    Tier = 2,
                    Cost = 1200,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Haste: 30);
                    },
                    ListStats = new List<StatType>() { StatType.Haste },
                    Parent = new string[] {"Time"},
                    Child = new string[] {"Allure", "Influence", "Corruption"},
                    PassiveName = "ZealPassive",
                    PassiveDesc = "Stackable: Dealing damage to an enemy forger marks them for 4 seconds. The next allied Forger to damage them within that timeframe gains 20 bonus movement speed for 2 seconds and the mark is consumed. This effect can happen once every 16 seconds.",
                    StackablePassive = ZealPassive
                },
                new Item() {
                    Name = "Fate",
                    Tier = 2,
                    Cost = 1500,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Haste: 35);
                    },
                    ListStats = new List<StatType>() { StatType.Haste },
                    Parent = new string[] {"Time"},
                    Child = new string[] {"Entropy", "Prophecy", "Insight"},
                    PassiveName = "FatePassive",
                    PassiveDesc = "UNIQUE: Dealing damage to an enemy slows them by 15% for 1 second. This can only effect each enemy once every 4 seconds.",
                    UniquePassive = FatePassive
                },
                new Item() {
                    Name = "Consumption",
                    Tier = 2,
                    Cost = 1500,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 25, Lifedrain: 10);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Lifedrain },
                    Parent = new string[] {"Hunger"},
                    Child = new string[] {"Voracity", "Gluttony"},
                    PassiveName = "",
                    PassiveDesc = ""
                },
                new Item() {
                    Name = "Desire",
                    Tier = 2,
                    Cost = 1200,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Armor: 25, Lifedrain: 5);
                    },
                    ListStats = new List<StatType>() { StatType.Armor, StatType.Lifedrain },
                    Parent = new string[] {"Hunger"},
                    Child = new string[] {"Avarice", "Envy", "Lust"},
                    PassiveName = "DesirePassive",
                    PassiveDesc = "Stackable: Your basic attacks grant you a stack of <heal thing> for 3 seconds, up to a maximum of 3. Your basic attacks to deal 5 bonus physical damage per stack.  You heal for 30% of the bonus damage. The healing is halved for ranged forgers.",
                    StackablePassive = DesirePassive
                },
                new Item() {
                    Name = "Defiance",
                    Tier = 3,
                    Cost = 3300,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 550, HealthRegen: 25, Haste: 25);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.HealthRegen, StatType.Haste },
                    Parent = new string[] {"Vitality"},
                    Child = null,
                    PassiveName = "DefiancePassive",
                    PassiveDesc = "UNIQUE: Upon reaching 30% of your maximum health, rapidly heal for 30% of your maximum health over the next 5 seconds. Cooldown of 80 seconds.",
                    UniquePassive = DefiancePassive
                },
                new Item() {
                    Name = "Renewal",
                    Tier = 3,
                    Cost = 2700,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 350, HealthRegen: 35, Haste: 20);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.HealthRegen, StatType.Haste },
                    Parent = new string[] {"Vitality"},
                    Child = null,
                    PassiveName = "RenewalPassive",
                    PassiveDesc = "UNIQUE: Increases the effectiveness of all healing effects on you by 15%. While in combat with enemy forgers, this effect increases to 50% over the course of 4 seconds. \n \nStackable: Taking damage from a large monster or forger causes you to regenerate 25 hp over 5 seconds. Has a cooldown of 20 seconds.",
                    UniquePassive = RenewalPassive,
                    StackablePassive = LifePlusPassive
                },
                new Item() {
                    Name = "Rampancy",
                    Tier = 3,
                    Cost = 3300,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 615, HealthRegen: 15);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.HealthRegen },
                    Parent = new string[] {"Vitality"},
                    Child = null,
                    PassiveName = "RampancyPassive",
                    PassiveDesc = "UNIQUE: You gain bonus power equal to 2.5% of your bonus Health. \n \nStackable: Taking damage from a large monster or forger causes you to regenerate 25 hp over 5 seconds. Has a cooldown of 20 seconds.",
                    UniquePassive = RampancyPassive,
                    StackablePassive = LifePlusPassive
                },
                new Item() {
                    Name = "Tenacity",
                    Tier = 3,
                    Cost = 2700,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 475, Haste: 25);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Haste },
                    Parent = new string[] {"Vigor"},
                    Child = null,
                    PassiveName = "TenacityPassive",
                    PassiveDesc = "UNIQUE: Reduce the effectiveness of all CCs on you by 35%. \n \nStackable: You gain 15 movement speed for 1 second whenever a CC expires from you.",
                    UniquePassive = TenacityPassive,
                    StackablePassive = VigorPassive
                },
                new Item() {
                    Name = "Endurance",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 250, Power: 20, Haste: 30);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Power, StatType.Haste },
                    Parent = new string[] {"Vigor"},
                    Child = null,
                    PassiveName = "EndurancePassive",
                    PassiveDesc = "UNIQUE: Every 30 seconds, gain a shield that prevents the first movement disabling effect used against you. \n \nStackable: You gain 15 movement speed for 1 second whenever a CC expires from you.",
                    UniquePassive = EndurancePassive,
                    StackablePassive = VigorPassive
                },
                new Item() {
                    Name = "Sanctuary",
                    Tier = 3,
                    Cost = 2700,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 300, Armor: 25);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Armor },
                    Parent = new string[] {"Sympathy"},
                    Child = null,
                    PassiveName = "SanctuaryPassive",
                    PassiveDesc = "UNIQUE: You and your nearby allies gain 20 Armor and 20 Magic Resistance. Casting a spell causes all allied forgers in a moderate area to be healed for 30 (+ .1 pow). Allies that are healed in this way also gain an additional 20 Armor and 20 Magic Resistance for 1.5 seconds. This effect has a 10 second cooldown.",
                    UniquePassive = SanctuaryPassive
                },
                new Item() {
                    Name = "Empathy",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 400, Armor: 60, Resistance: 50);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Armor, StatType.Resistance },
                    Parent = new string[] {"Sympathy"},
                    Child = null,
                    PassiveName = "EmpathyPassive",
                    PassiveDesc = "UNIQUE: Redirect 15% of the damage that your nearest ally would take to yourself. This effect deactivates upon you reaching 25% of your health or less.",
                    UniquePassive = EmpathyPassive
                },
                new Item() {
                    Name = "Charity",
                    Tier = 3,
                    Cost = 2700,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 325, Haste: 25);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Haste },
                    Parent = new string[] {"Sympathy"},
                    Child = null,
                    PassiveName = "CharityPassive",
                    PassiveDesc = "UNIQUE: Heals and shields you use on allies have their effectiveness increased by 1% for each 2% HP that ally is missing. \n \nStackable: Every 15 seconds, you heal the lowest % health nearby ally for 25 health.",
                    UniquePassive = CharityPassive,
                    StackablePassive = SympathyPassive
                },
                new Item() {
                    Name = "Synergy",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 350, Power: 40);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Power },
                    Parent = new string[] {"Sympathy"},
                    Child = null,
                    PassiveName = "SynergyPassive",
                    PassiveDesc = "UNIQUE: Your basic attacks and single target abilities mark enemy forgers. Allies that deal damage to the marked forger deal an additional 85 (+ .3 power) magic damage over the next 3 seconds and gain a 50 (+ .2 power) shield. Only one enemy forger may be marked at a time, and allies attacking the same forger reset the damage timer. Allies can only gain the benefit of this effect once every 10 seconds.",
                    UniquePassive = SynergyPassive
                },
                new Item() {
                    Name = "Maturity",
                    Tier = 3,
                    Cost = 2700,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 300, Armor: 15, Resistance: 15);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Armor, StatType.Resistance },
                    Parent = new string[] {"Growth"},
                    Child = null,
                    PassiveName = "MaturityPassive",
                    PassiveDesc = "UNIQUE: Permanently gain 30 Health, 3 Armor, and 2 Magic Resistance every 75 seconds. This caps at 300 Health, 30 Armor, 20 Magic Resistance. \n \nStackable: Whenever you level up, you regain 250 health over the next 10 seconds.",
                    StackablePassive = GrowthPassive,
                    UniquePassive = MaturityPassive
                },
                new Item() {
                    Name = "Proliferation",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 275, Armor: 45, Haste: 30);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Armor, StatType.Haste },
                    Parent = new string[] {"Growth"},
                    Child = null,
                    PassiveName = "ProliferationPassive",
                    PassiveDesc = "UNIQUE: Your basic attacks deal an additional 10 true damage to structures for each nearby allied minion. This effect caps at 90 bonus true damage. \n \nStackable: Whenever you level up, you regain 250 health over the next 10 seconds.",
                    StackablePassive = GrowthPassive,
                    UniquePassive = ProliferationPassive
                },
                new Item() {
                    Name = "Law",
                    Tier = 3,
                    Cost = 2400,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Armor: 85);
                    },
                    ListStats = new List<StatType>() { StatType.Armor },
                    Parent = new string[] {"Integrity"},
                    Child = null,
                    PassiveName = "LawPassive",
                    PassiveDesc = "UNIQUE: Reduce the damage taken by basic attacks by 20%. If you are damaged by a critical hit, the attacker is slowed by 70% which rapidly decays over .5 seconds and the reduction is increased to 30%. This increased effect can only happen once every 5 seconds.",
                    UniquePassive = LawPassive
                },
                new Item() {
                    Name = "Gravity",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Armor: 60, Power: 40);
                    },
                    ListStats = new List<StatType>() { StatType.Armor, StatType.Power },
                    Parent = new string[] {"Substance"},
                    Child = null,
                    PassiveName = "GravityPassive",
                    PassiveDesc = "UNIQUE: Taking damage builds a stack of “lockdown” for 4 seconds. At 5 stacks, lockdown activates, reducing all nearby enemies’ movement speed by 35% for 1 second. \n \nStackable: Your abilities deal 10% of your bonus armor as extra damage. This is 33% as effective on damage over time abilities, and each additional instance of this after the first is 50% as effect.",
                    UniquePassive = GravityPassive,
                    StackablePassive = SubstancePassive
                },
                new Item() {
                    Name = "Pride",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Armor: 50, Power: 40);
                    },
                    ListStats = new List<StatType>() { StatType.Armor, StatType.Power },
                    Parent = new string[] {"Substance"},
                    Child = null,
                    PassiveName = "PridePassive",
                    PassiveDesc = "UNIQUE: Dealing damage to enemy forgers with an ability reduces physical damage taken by 10% for 4 seconds. This effect can stack up to twice. This effect has a 30 second cooldown, or it refreshes after you are out of combat for 10 seconds. \n\nStackable: Your abilities deal 10% of your bonus armor as extra damage. This is 33% as effective on damage over time abilities, and each additional instance of this after the first is 50% as effect.",
                    UniquePassive = PridePassive,
                    StackablePassive = SubstancePassive
                },
                new Item() {
                    Name = "Subjugation",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Armor: 65, Haste: 30);
                    },
                    ListStats = new List<StatType>() { StatType.Armor, StatType.Haste },
                    Parent = new string[] {"Restraint"},
                    Child = null,
                    PassiveName = "SubjugationPassive",
                    PassiveDesc = "UNIQUE: Dealing damage to an enemy reduces their Power by 15 and their attack speed by 15% for 2 seconds. This effect can stack up to two times.",
                    UniquePassive = SubjugationPassive
                },
                new Item() {
                    Name = "Suppression",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Armor: 50, Resistance: 30, Haste: 30);
                    },
                    ListStats = new List<StatType>() { StatType.Armor, StatType.Resistance, StatType.Haste },
                    Parent = new string[] {"Restraint"},
                    Child = null,
                    PassiveName = "SuppressionPassive",
                    PassiveDesc = "UNIQUE: Dealing damage to an enemy reduces their ability damage output by 20% for 2 seconds.",
                    UniquePassive = SuppressionPassive
                },
                new Item() {
                    Name = "Hope",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Armor: 55, Resistance: 45);
                    },
                    ListStats = new List<StatType>() { StatType.Armor, StatType.Resistance },
                    Parent = new string[] {"Protection"},
                    Child = null,
                    PassiveName = "HopePassive",
                    PassiveDesc = "UNIQUE: Whenever you take damage that would put you below 30% of your maximum health, you gain 15% Damage Reduction and a shield that blocks 100 (+20 per level) damage. This effect lasts for 3 seconds. this effect has a 10 second cooldown.",
                    UniquePassive = HopePassive
                },
                new Item() {
                    Name = "Adaptation",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats= (Unit owner) => {
                        return UnitStats.Stats(Armor: 65, Resistance: 65);
                    },
                    ListStats = new List<StatType>() { StatType.Armor, StatType.Resistance },
                    Parent = new string[] {"Protection"},
                    Child = null,
                    PassiveName = "AdaptationPassive",
                    PassiveDesc = "UNIQUE: You gain 20 armor and 20 resistance. Upon taking damage, the proportion between these stats is adjusted based on the damage type taken: Taking physical damage increases the amount of armor provided by 5 while decreasing the resistance by the same amount, while taking magical damage increases the resistance by 5provided while decreasing the armor by the same amount. This effect cannot grant more than 40 armor or 40 resistance.",
                    UniquePassive = AdaptationPassive
                },
                new Item() {
                    Name = "Resolve",
                    Tier = 3,
                    Cost = 3300,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 350, Resistance: 55, Power: 30);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Resistance, StatType.Power },
                    Parent = new string[] {"Perseverance"},
                    Child = null,
                    PassiveName = "ResolvePassive",
                    PassiveDesc = "UNIQUE: You gain additional power based on your missing health, up to a maximum of 35, at 30% of your maximum health. \n\nStackable: Whenever you take magic damage that would put you at lower than 30% of your maximum health, gain a shield that lasts for 4 seconds and absorbs 200 magic damage. If you take magic damage while the shield is active, the duration of the shield is increased by 1 second, to a maximum of 8 seconds. This has a 45 second cooldown. Each additional instance of this passive is 50% less effective.",
                    UniquePassive = ResolvePassive,
                    StackablePassive = PerseverancePassive
                },
                new Item() {
                    Name = "Retribution",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 400, Resistance: 40, Haste: 25);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Resistance, StatType.Haste },
                    Parent = new string[] {"Perseverance"},
                    Child = null,
                    PassiveName = "RetributionPassive",
                    PassiveDesc = "UNIQUE: Whenever you drop below 30% of your maximum health, all of your basic ability cooldowns are fully refreshed. This effect has a 15 second cooldown. \n\nStackable: Whenever you take magic damage that would put you at lower than 30% of your maximum health, gain a shield that lasts for 4 seconds and absorbs 200 magic damage. If you take magic damage while the shield is active, the duration of the shield is increased by 1 second, to a maximum of 8 seconds. This has a 45 second cooldown. Each additional instance of this passive is 50% less effective.",
                    UniquePassive = RetributionPassive,
                    StackablePassive = PerseverancePassive
                },
                new Item() {
                    Name = "Focus",
                    Tier = 3,
                    Cost = 2700,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 350, Resistance: 70);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Resistance },
                    Parent = new string[] {"Perseverance"},
                    Child = null,
                    PassiveName = "FocusPassive",
                    PassiveDesc = "UNIQUE: You gain a shield that blocks (100-350 based on level) magic damage if you do not take magic damage for 10 seconds.",
                    UniquePassive = FocusPassive
                },
                new Item() {
                    Name = "Arrogance",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Resistance: 35, Power: 40);
                    },
                    ListStats = new List<StatType>() { StatType.Resistance, StatType.Power },
                    Parent = new string[] {"Confidence"},
                    Child = null,
                    PassiveName = "ArrogancePassive",
                    PassiveDesc = "UNIQUE: Dealing damage to an enemy forger reduces their Magic Resistance by 5. This can stack 3 times to a maximum of 15 Magic Resistance reduction per enemy forger. You gain Magic Resistance equal to the total amount of shredded Magic Resistance, up to a maximum of 75 Magic Resistance.",
                    UniquePassive = ArrogancePassive
                },
                new Item() {
                    Name = "Courage",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Resistance: 35, Power: 25, Haste: 30);
                    },
                    ListStats = new List<StatType>() { StatType.Resistance, StatType.Power, StatType.Haste },
                    Parent = new string[] {"Confidence"},
                    Child = null,
                    PassiveName = "CouragePassive",
                    PassiveDesc = "UNIQUE: Your basic attacks deal 50 bonus magic damage. \n\nStackable: Whenever you deal damage to an enemy unit, reduce their Magic Resistance by 3 for 5 seconds. This stacks up to 4 times.",
                    UniquePassive = CouragePassive,
                    StackablePassive = ConfidencePassive
                },
                new Item() {
                    Name = "Rebellion",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 60);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Penetration },
                    Parent = new string[] {"Authority"},
                    Child = null,
                    PassiveName = "RebellionPassive",
                    PassiveDesc = "UNIQUE: You gain 30 Defense Penetration.",
                    UniquePassive = RebellionPassive
                },
                new Item() {
                    Name = "Dominance",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit stats) => {
                        return UnitStats.Stats(Power: 50);
                    },
                    ListStats = new List<StatType>() { StatType.Power },
                    Parent = new string[] {"Authority"},
                    Child = null,
                    PassiveName = "DominancePassive",
                    PassiveDesc = "UNIQUE: Damage you deal ignores 50% of your opponent’s bonus resistances.",
                    UniquePassive = DominancePassive
                },
                new Item() {
                    Name = "Justice",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 300, Power: 50, Penetration: 10);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Power, StatType.Penetration },
                    Parent = new string[] {"Authority"},
                    Child = null,
                    PassiveName = "JusticePassive",
                    PassiveDesc = "UNIQUE: Dealing damage reduces your opponent’s Armor or Magic Resistance by 5% for 4 seconds, depending on if the damage dealt was physical or magical. This effect stacks up to 5 times for a maximum reduction of 25%.",
                    UniquePassive = JusticePassive
                },
                new Item() {
                    Name = "Precision",
                    Tier = 3,
                    Cost = 3600,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 50, Mastery: 25);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Mastery },
                    Parent = new string[] {"Intensity"},
                    Child = null,
                    PassiveName = "PrecisionPassive",
                    PassiveDesc = "UNIQUE: Whenever you deal damage to an enemy with a critical hit, gain a stack of Static Charge, to a maximum of 4. Each subsequent basic attack causes 25% of the damage the attack deals to arc as magical damage to a number of enemies equal to the number of stacks of Static Charge you have built (prioritizing Forgers). This effect ends 8 seconds after you stop basic attacking.",
                    UniquePassive = PercisionPassive
                },
                new Item() {
                    Name = "Pain",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 45, Mastery: 20);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Mastery },
                    Parent = new string[] {"Intensity"},
                    Child = null,
                    PassiveName = "PainPassive",
                    PassiveDesc = "UNIQUE: Whenever you crit an enemy, you deal an additional (+1.5 Power) physical damage over a 2 second duration.",
                    UniquePassive = PainPassive
                },
                new Item() {
                    Name = "Force",
                    Tier = 3,
                    Cost = 3300,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 60, Mastery: 20);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Mastery },
                    Parent = new string[] {"Intensity"},
                    Child = null,
                    PassiveName = "ForcePassive",
                    PassiveDesc = "UNIQUE: Whenever you deal damage with an ability, you deal (+.3 Power) bonus magic damage. This bonus damage can only be applied once per spell.",
                    UniquePassive = ForcePassive
                },
                new Item() {
                    Name = "Command",
                    Tier = 3,
                    Cost = 2700,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 175, Power: 30, Mastery: 10);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Power, StatType.Mastery },
                    Parent = new string[] {"Intensity"},
                    Child = null,
                    PassiveName = "CommandPassive",
                    PassiveDesc = "UNIQUE: Whenever you cast a spell, you and your nearby allies gain 10 Power and 5 Mastery for 6 seconds. This can stack up to 3 times.",
                    UniquePassive = CommandPassive
                },
                new Item() {
                    Name = "Destruction",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 40, Mastery: 20);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Mastery },
                    Parent = new string[] {"Intensity"},
                    Child = null,
                    PassiveName = "DestructionPassive",
                    PassiveDesc = "UNIQUE: Whenever you deal damage to an enemy Forger with a single target spell, you deal additional magic damage equal to 30 (+.4 Power) plus 3 (+.1 Mastery)% of the Forger’s maximum health. This has a 6 second cooldown per enemy forger.",
                    UniquePassive = DestructionPassive
                },
                new Item() {
                    Name = "Fatigue",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 275, Power: 50);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Power },
                    Parent = new string[] {"Aggression"},
                    Child = null,
                    PassiveName = "FatiguePassive",
                    PassiveDesc = "UNIQUE: Whenever you deal ability damage to an enemy, they have a debuff applied to them that causes them to take 10% increased ability damage for 4 seconds.",
                    UniquePassive = FatiguePassive
                },
                new Item() {
                    Name = "Supremacy",
                    Tier = 3,
                    Cost = 3300,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 70);
                    },
                    ListStats = new List<StatType>() { StatType.Power },
                    Parent = new string[] {"Aggression"},
                    Child = null,
                    PassiveName = "SupremacyPassive",
                    PassiveDesc = "UNIQUE: Casting an ability or auto attacking grants you 30% increased power for 4 seconds. This effect has a 6 second cooldown.",
                    UniquePassive = SupremacyPassive
                },
                new Item() {
                    Name = "Wrath",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 40, Haste: 25);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Haste },
                    Parent = new string[] {"Aggression"},
                    Child = null,
                    PassiveName = "WrathPassive",
                    PassiveDesc = "UNIQUE: Whenever you cast an ability, your next basic attack within 6 seconds deals 50 (+1.2 Power) bonus physical damage. This effect has a 2 second cooldown.",
                    UniquePassive = WrathPassive
                },
                new Item() {
                    Name = "Desolation",
                    Tier = 3,
                    Cost = 2400,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 55);
                    },
                    ListStats = new List<StatType>() { StatType.Power },
                    Parent = new string[] {"Aggression"},
                    Child = null,
                    PassiveName = "DesolationPassive",
                    PassiveDesc = "UNIQUE: Whenever you deal damage to an enemy Forger, you deal an extra 30 (+.4 Power) magic damage to them and their healing is reduced by 35% for 3.5 seconds. This effect has a 6 second cooldown per enemy Forger.",
                    UniquePassive = DesolationPassive
                },
                new Item() {
                    Name = "Divinity",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 200, Power: 25);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Power },
                    Parent = new string[] {"Potential"},
                    Child = null,
                    PassiveName = "DivnityPassive",
                    PassiveDesc = "UNIQUE: Whenever you kill a unit, you gain 4 health and .5 power permanently. After you gain 40 stacks of this, the bonus stats gained decrease by 1% for each additional stack. Stacks gained from Potential carry over into this item’s passive.",
                    UniquePassive = DivinityPassive
                },
                new Item() {
                    Name = "Conquest",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 225, Power: 25);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Power },
                    Parent = new string[] {"Potential"},
                    Child = null,
                    PassiveName = "ConquestPassive",
                    PassiveDesc = "UNIQUE: Whenever you kill a minion, monster, or well worker, you gain a stack of [stack name], capping at 8 stacks. You also gain 1 stack every 10 seconds. Whenever you deal damage to an enemy forger with a basic attack or single target spell, you deal an additional 30 (+.15 Power per stack) magic damage, and all stacks of [stack name] are consumed. \n\nPassive: Killing a minion, monster, or miner you gain 4 bonus Health and .5 bonus Power permanently. This stacks up to 40 times. You carry over your stacks from the Potential.",
                    UniquePassive = ConquestPassive
                },
                new Item() {
                    Name = "Ruin",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 20, Haste: 35);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Haste },
                    Parent = new string[] {"Decay"},
                    Child = null,
                    PassiveName = "RuinPassive",
                    PassiveDesc = "UNIQUE: Your auto attacks deal an additional 25 (+12% of target's bonus health) as physical damage.",
                    UniquePassive = RuinPassive
                },
                new Item() {
                    Name = "Turmoil",
                    Tier = 3,
                    Cost = 2700,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 25, Haste: 30);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Haste },
                    Parent = new string[] {"Decay"},
                    Child = null,
                    PassiveName = "TurmoilPassive",
                    PassiveDesc = "UNIQUE: Your basic attacks hit up to 3 additional nearby targets. These extra attacks deal 40% of the initial attack’s damage and apply all on hit effects. \n\nStackable: Your basic attacks deal an additional 20 bonus physical damage.",
                    UniquePassive = TurmoilPassive,
                    StackablePassive = DecayPassive
                },
                new Item() {
                    Name = "Momentum",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (unit) => {
                        return UnitStats.Stats(Power: 15, Haste: 35);
                    },
                    PercentItemStats = UnitStats.Stats(MovementSpeed:5),
                    ListStats = new List<StatType>() { StatType.Power, StatType.Haste, StatType.PercentMovementSpeed},
                    Parent = new string[] {"Decay"},
                    Child = null,
                    PassiveName = "MomentumPassive",
                    PassiveDesc = "UNIQUE: Your basic attacks deal an additional 5% of their damage as true damage, calculated pre-mitigation. In addition, every 8 seconds, your next basic attack gains 20% increased attack range. If this basic attack targets an enemy forger, the cooldown on this effect is fully refreshed and your next basic attack deals 10% of its damage as true damage instead, calculated pre-mitigation."
                },
                new Item() {
                    Name = "Dissonance",
                    Tier = 3,
                    Cost = 3300,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 25, Mastery: 25, Haste: 25);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Mastery, StatType.Haste },
                    Parent = new string[] {"Decay"},
                    Child = null,
                    PassiveName = "DissonancePassive",
                    PassiveDesc = "UNIQUE: UNIQUE: Whenever you basic attack with a critical hit, you do 2 attacks that deal 110% damage and apply all on hits instead of dealing 200% damage. The second attack does not count towards building your crit meter. \n\nStackable: Your basic attacks deal an additional 20 bonus physical damage.",
                    UniquePassive = DissonancePassive,
                    StackablePassive = DecayPassive
                },
                new Item() {
                    Name = "Fury",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 40, Haste: 30);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Haste },
                    Parent = new string[] {"Recurrence"},
                    Child = null,
                    PassiveName = "FuryPassive",
                    PassiveDesc= "UNIQUE: Dealing damage to an enemy with an ability burns the enemy for 5% of their maximum health over a 3 second duration.",
                    UniquePassive = FuryPassive
                },
                new Item() {
                    Name = "Finesse",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 10, Mastery: 30, Haste: 30);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Mastery, StatType.Haste },
                    Parent = new string[] {"Recurrence"},
                    Child = null,
                    PassiveName = "FinessePassive",
                    PassiveDesc = "UNIQUE: Dealing damage to an enemy Forger or Large Monster with an ability causes power to begin gathering in that location. After .75 seconds, the area erupts, dealing 60 (+.4 Power) magic damage to all enemies within the area. This damage is increased by 15 (+.1 Power) for each unique enemy Forger or Large Monster damaged by the player within that duration, up to a cap of 120 (+.8 Power). This has a 6 second cooldown.",
                    UniquePassive = FinessePassive
                },
                new Item() {
                    Name = "Resonance",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 275, Power: 25, Haste: 35);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Power, StatType.Haste },
                    Parent = new string[] {"Recurrence"},
                    Child = null,
                    PassiveName = "ResonancePassive",
                    PassiveDesc = "UNIQUE: Whenever a nearby ally deals damage to an enemy, your basic ability cooldowns are reduced by .25 seconds. This effect cannot happen more than once every 1 second.",
                    UniquePassive = ResonancePassive
                },
                new Item() {
                    Name = "Continuity",
                    Tier = 3,
                    Cost = 2700,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Power: 25, Haste: 35);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Haste },
                    Parent = new string[] {"Recurrence"},
                    Child = null,
                    PassiveName = "ContinuityPassive",
                    PassiveDesc = "UNIQUE: Your basic attacks reduce the cooldowns of your basic abilities by .25 seconds. This effect is doubled against enemy forgers. This has a .75 second cooldown.",
                    UniquePassive = ContinuityPassive
                },
                new Item() {
                    Name = "Allure",
                    Tier = 3,
                    Cost = 2700,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 300, Haste: 35);
                    },
                    PercentItemStats = UnitStats.Stats(MovementSpeed:10),
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Haste, StatType.PercentMovementSpeed },
                    Parent = new string[] {"Zeal"},
                    Child = null,
                    PassiveName = "AllurePassive",
                    PassiveDesc = "UNIQUE: Nearby allies gain 10% movement speed when moving towards you. This effect is doubled for your allies while they are out of combat for 5 seconds.",
                    UniquePassive = AllurePassive
                },
                new Item() {
                    Name = "Influence",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 275, Haste: 30);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Haste },
                    Parent = new string[] {"Zeal"},
                    Child = null,
                    PassiveName = "InfluencePassive",
                    PassiveDesc = "UNIQUE: Dealing damage to an enemy Forger with an auto attack marks them for 6 seconds. Nearby allies that attack the marked target for 3 consecutive auto attacks perform an additional basic attack at 50% damage on the third auto attack and gain 30 bonus movement speed for 2 seconds.",
                    UniquePassive = InfluencePassive
                },
                new Item() {
                    Name = "Corruption",
                    Tier = 3,
                    Cost = 2400,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(MaxHealth: 275, Haste: 35);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Haste },
                    Parent = new string[] {"Zeal"},
                    Child = null,
                    PassiveName = "CorruptionPassive",
                    PassiveDesc = "UNIQUE: Dealing damage to an enemy forger marks them for 3 seconds. When they are marked, their healing is reduced by 35%. Allied Forgers that deal damage to them within that timeframe gains 20 bonus movement speed for 2 seconds. Each ally can only benefit from this once per mark. Enemies cannot be effected by this more than once every 5 seconds.",
                    UniquePassive = CorruptionPassive
                },
                new Item() {
                    Name = "Entropy",
                    Tier = 3,
                    Cost = 2700,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Name: "EntropyStats", Power: 35, Haste: 35);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Haste },
                    Parent = new string[] {"Fate"},
                    Child = null,
                    PassiveName = "Entropy passive",
                    PassiveDesc = "UNIQUE: Dealing damage to an enemy consumes a charge of {thing}, which increases your movement speed by 60 for .75 seconds. It also slows the enemy’s movement speed by 60 for .75 seconds. Both the speed boost and the slow decay rapidly. You build 1 charge every 5 seconds, up to a maximum of 4 charges.",
                    UniquePassive = EntropyPassive
                },
                new Item() {
                    Name = "Prophecy",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Name: "ProphecyStats", MaxHealth: 300, Haste: 40);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Haste},
                    Parent = new string[] {"Fate"},
                    Child = null,
                    PassiveName = "Prophecy passive",
                    PassiveDesc = "UNIQUE: Dealing damage to an enemy slows them by a specific amount, based on the way the damage was dealt. Over the next .75 seconds, this slow increases to 70%, and remains at 70% for .25 seconds. This has a 5 second cooldown for each enemy. Ranged attacks: 25% Melee attacks: 40% Single target: 35% Aoe: 15%",
                    UniquePassive = ProphecyPassive
                },
                new Item() {
                    Name = "Insight",
                    Tier = 3,
                    Cost = 2700,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Name: "InsightStats", Haste: 40);
                    },
                    ListStats = new List<StatType>() { StatType.Haste },
                    Parent = new string[] {"Fate"},
                    Child = null,
                    PassiveName = "Insight passive",
                    PassiveDesc = "UNIQUE: Dealing damage to an enemy forger with an auto attack applies a stack of Awareness. For the next 4 seconds, your basic attacks and allied basic attacks that deal damage to the target will add an additional stack of Awareness (2 for melee auto attacks). When a target reaches 5 stacks of Awareness, they are rooted for 1 second. After a target has been rooted, they can not be rooted for 10 seconds.",
                    UniquePassive = InsightPassive
                },
                new Item() {
                    Name = "Voracity",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Name: "VoracityStats", Power:50, Haste: 25, Lifedrain: 10);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Haste, StatType.Lifedrain },
                    Parent = new string[] {"Consumption"},
                    Child = null,
                    PassiveName = "Voracity passive",
                    PassiveDesc = "UNIQUE: Dealing damage with basic attacks grant 3% lifedrain. This stacks up to 5 times for a maximum of 15% additional lifedrain. Damaging an enemy forger with a basic attack instead grants you two stacks.",
                    UniquePassive = VoracityPassive
                },
                new Item() {
                    Name = "Gluttony",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Name: "GluttonyStats", Power: 50, Lifedrain: 15);
                    },
                    ListStats = new List<StatType>() { StatType.Power, StatType.Lifedrain },
                    Parent = new string[] {"Consumption"},
                    Child = null,
                    PassiveName = "Gluttony passive",
                    PassiveDesc = "UNIQUE: You heal for 15% of the damage that your abilities would deal pre-mitigation. This effect is reduced to 33% effectiveness on each target for area of effect abilities.",
                    UniquePassive = GluttonyPassive
                },
                new Item() {
                    Name = "Avarice",
                    Tier = 3,
                    Cost = 3000,
                    ItemStats = (Unit owner) => {
                        return UnitStats.Stats(Name: "AvariceStats", MaxHealth: 375, Armor: 40, Lifedrain: 10);
                    },
                    ListStats = new List<StatType>() { StatType.MaxHealth, StatType.Armor, StatType.Lifedrain },
                    Parent = new string[] {"Desire"},
                    Child = null,
                    PassiveName = "Avarice passive",
                    PassiveDesc = "UNIQUE: You deal 1.5% of your maximum health as magical damage to all nearby enemies each second. You heal for 10% of the damage being dealt, and this healing is doubled against forgers. You cannot heal for more than .75% of your maximum health each second in this way.",
                    UniquePassive = AvaricePassive
                },
                new Item() {
                    Name = "Envy",
                }, 
                new Item() {
                    Name = "Lust",
                }
            };

            List<Item> secondTier = new List<Item>();

            foreach(var item in firstTier.TopLevelItems)
            {
                if (!Items.ContainsKey(item.name))
                {
                    Items.Add(item.name, item);
                    secondTier.AddRange(item.BuildsInto);
                }
            }

            List<Item> thirdTier = new List<Item>();

            foreach(var item in secondTier)
            {
                if (!Items.ContainsKey(item.name))
                {
                    Items.Add(item.name, item);
                    thirdTier.AddRange(item.BuildsInto);
                }
            }

            foreach (var item in thirdTier)
            {
                if (!Items.ContainsKey(item.name))
                {
                    Items.Add(item.name, item);
                }
            }

            return Items;
        }
        void Update()
        {

        }

    }
}