﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;


namespace feature
{
	[CreateAssetMenu(fileName = "Perseverance Passive", menuName = "Passives/Items/Perseverance")]
	public class PerseverancePassive : Passive
	{
		[SerializeField]
		float PerseverancePassiveHealthTriggerPercentage;
		[SerializeField]
		BuffDebuff PerseveranceBuff;
		
		public PerseverancePassive()
		{
			Name = "Perseverance";

			BeforeTakeDamagePostMit = (SourcePair pair, Unit owner, PassiveTracker data, DamagePacket packet) =>
			{
				float packetMagicDamage = packet.PostMitigationMagical;
				float percentRemainingHpAfterDamage = (owner.Health - packet.TotalDamage) / owner.MaxHealth;
				if(packetMagicDamage > 0 && percentRemainingHpAfterDamage <= PerseverancePassiveHealthTriggerPercentage)
				{
					owner.AddBuff(PerseveranceBuff.WithSource(pair.Source));
					//TODO: Add shield buff
				}
				return packet;
			};
		}
	}
}