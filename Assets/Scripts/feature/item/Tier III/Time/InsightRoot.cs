﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "Insight Root", menuName = "Passives/Items/Insight Root")]
    public class InsightRoot : Status
    {
        [SerializeField]
        float InsightImmunityDuration = 10;
        [SerializeField]
        int InsightDebuffStackPerRanged = 1;
        [SerializeField]
        int InsightDebuffStackPerMelee = 2;
        [SerializeField]
        float InsightDebuffDuration = 4;
        [SerializeField]
        int InsightDebuffMaxStacks = 5;
        [SerializeField]
        float InsightRootDuration = 1;

        [SerializeField]
        BuffDebuff InsightImmunity;
        [SerializeField]
        BuffDebuff InsightDebuff;
        [SerializeField]
        BuffDebuff InsightDebuffTimer;



        public InsightRoot()
        {
            
        }
    }
}