﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "Entropy Speed Buff", menuName = "Passives/Items/Entropy Speed Buff")]
    public class EntropySpeedBuff : BuffDebuff
    {
        public int EntropyBuffMaxStacs = 4;
        public float EntropyBuffChargeCooldown = 5;
        public float EntropySpeedBuffAmount = 60;
        public float EntropySpeedBuffDuration = .75f;
        public float EntropySlowAmount = 60;
        public float EntropySlowDuration = .75f;

        public EntropySpeedBuff()
        {
            DynamicStats = (pair, holder, data) => {
                /*  // Trying without stacking for now. 
                float totalSpeedUp = pair.ability.StackValue(EntropySpeedBuffAmount, 1, holder.StacksOf(pair) - 1);
                float adjustedSpeedUp = totalSpeedUp * (1f - data.TimeSliceRatio);
                */
                float adjustedSpeedUp = EntropySpeedBuffAmount * (1f - data.TimeSliceRatio);
                return UnitStats.Stats(MovementSpeed: adjustedSpeedUp);
            };
            OnTick = (pair, holder, data, deltaTime) =>
            {
                holder.RecalculateDynamicStats(pair);
            };
        }
    }
}