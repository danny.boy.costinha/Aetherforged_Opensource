﻿using UnityEngine;
using core;


namespace feature
{
    [CreateAssetMenu(fileName = "Corruption Speed Buff", menuName = "Passives/Items/Corruption speed Buff")]
    public class CorruptionSpeedBuff : BuffDebuff
    {
        public float CorruptionImmunityDuration = 5;
        public float CorruptionDebuffDuration = 3;
        public float CorruptionSpeedBuffDuration = 2;
        public float CorruptionSpeedBuffIncrease = 20;


        [SerializeField] BuffDebuff CorruptionDebuff;



        public CorruptionSpeedBuff()
        {
            Name = "Corruption speed buff";
            AlsoObsolete_Duration = (pair) =>
            {
                return CorruptionSpeedBuffDuration;
            };
            DynamicStats = (SourcePair pair, Unit owner, PassiveTracker data) =>
            {
                return UnitStats.Stats(MovementSpeed: CorruptionSpeedBuffIncrease);
            };
        }
    }
}