﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using core;
using core.forger;
using managers;

namespace feature
{
    [CreateAssetMenu(fileName = "ResistancePassive", menuName = "Passives/Items/Resistance")]
    public class ResistancePassive : Passive
    {
        public float[] ResistancePassiveCooldown = new float[] { 5 };
        public float ResistancePassiveDamageBlock = 15;
        public float ResistancePassiveEffectivenessAfterFirst = .5f;




        public ResistancePassive()
        {
			Name = "Resistance passive";
			Cooldowns = ResistancePassiveCooldown;
			BeforeTakeDamage = (SourcePair pair, Unit owner, PassiveTracker data, DamagePacket packet) => {
				float finalDamagetoReduce = pair.ability.StackValue(ResistancePassiveDamageBlock, ResistancePassiveEffectivenessAfterFirst, owner.StacksOf(pair) - 1);
				if (packet.MagicalDamage > 0) {
					packet.FlatMagicalDamageReduction += finalDamagetoReduce;
					owner.StartBareCooldown(pair);
				}
				return packet;
			};
			NoMaxStacks = true;
		}
    }
}