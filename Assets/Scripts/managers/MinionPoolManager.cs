using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using core;
using core.lane;
using controllers.ai;

namespace managers
{
public class MinionPoolManager {
	static int number = 144;
	static Transform poolRoot;

	static GameObject[] pool;

	static int lastIndex = 0;

	// Use this for initialization
	public static void Initialize (Transform rootTransform) {
		pool = new GameObject [number];
		poolRoot = rootTransform;

		for (int index = 0; index < number; index++) {
			pool[index] = FreshMinion();
		}
		
	}

	static GameObject FreshMinion () {
		GameObject gob = GameObject.Instantiate(Resources.Load("UnitMinion") as GameObject);
		var minion = gob.GetComponent<Minion>();

		Debug.Assert(minion != null);

		gob.transform.SetParent(poolRoot);
		gob.SetActive(false);
		return gob;
	}

	public static Minion SpawnMinion(Team team, Lane spawnLane) {
		var foundSpare = false;
		GameObject miniGob = null;
		if (lastIndex == number) {
			lastIndex = 0;
		}
		for (int index = lastIndex; index < number; index++) {
			if (pool [index].activeInHierarchy) {
				continue;   // already active, give it a pass.
			}
		
			miniGob = pool [index];
			foundSpare = true;
			break;
		}

		if (!foundSpare) {
			miniGob = FreshMinion();
		}


		Debug.Assert(miniGob != null);
		var ai = miniGob.GetComponent<MinionAI>();

        ai.ClearBrain();


		var minionSpawn = 
			GameObject.Find(team.ToString() + spawnLane.ToString());

		miniGob.transform.position = minionSpawn.transform.position;
		//			ai.AddWaypoint(minionSpawn.transform.position);
		//			for (int i = 1; i <= minionSpawn.transform.childCount; i++) {
		//				ai.AddWaypoint(minionSpawn.transform.FindChild("Waypoint" + i).transform.position);
		//			}

		var minion = miniGob.GetComponent<Minion>();

		Debug.Assert(minion != null);

		minion.Team = team;
		miniGob.SetActive(true);
		minion.Spawn();
		minion.aliveTime = GameTime.time;

		return minion;
	}
}
}
