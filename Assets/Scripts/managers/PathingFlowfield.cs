﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using core;
using core.utility;
using core.collection;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

[CreateAssetMenu(fileName = "Flowfield", menuName = "Pathing/Flowfield")]
public class PathingFlowfield : ScriptableObject
{

    [SerializeField]
    public List<FieldNodeTuple> vectorList;

    Dictionary<Vector2, Vector2> vectors = new Dictionary<Vector2, Vector2>();

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public FieldNodeTuple NearestNode(Vector2 position) {
        
        var rankThisDist = new MinHeap<float, int>();

        for (int index = 0; index < vectorList.Count; ++index)
        {
            rankThisDist.Add(Vector2.Distance(vectorList[index].position, position), index);
        }

        if (rankThisDist.Count > 0) {
            var index = rankThisDist.Top();
            return vectorList[index];
        }
        return null;
    }
}


[System.Serializable]
public class FieldNodeTuple
{

    [SerializeField]
    public Vector2Int position;
    [SerializeField]
    public Vector2 direction;

}

#if UNITY_EDITOR
[CustomEditor(typeof(PathingFlowfield))]
public class FlowfieldPathingEditor : Editor
{
    
    private ReorderableList nodeList;

    private void OnEnable()
    {
        nodeList = new ReorderableList(serializedObject,
                                       serializedObject.FindProperty("vectorList"),
                                       true, true, true, true);

        nodeList.drawHeaderCallback = (rect) =>
        {
            EditorGUI.LabelField(rect, "Field Vector Nodes");
        };

        nodeList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            var element = nodeList.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;

            float buttonWidth = 32f;
            float spaceWidth = 5f;
            float labelWidth = 52f;
            float directionWidth = 140f;
            float nameWidth = rect.width - directionWidth - spaceWidth *4 - labelWidth - buttonWidth;

            //EditorGUILayout.LabelField("position");
            //EditorGUILayout.Vector2Field("again", element.FindPropertyRelative("position").vector2Value);

            if (GUI.Button(
                new Rect(rect.x, rect.y, buttonWidth, EditorGUIUtility.singleLineHeight),
                "go"
            ))
            {
                GameObject gob = new GameObject("_temp_");
                var position = element.FindPropertyRelative("position").vector2IntValue;

                Vector3 v3position = new Vector3(position.x, 5, position.y) * Constants.TO_UNITY_UNITS;
                gob.transform.position = v3position;
                var saveObjects = Selection.objects;
                Selection.activeObject = gob;
                SceneView.lastActiveSceneView.FrameSelected();
                Selection.objects = saveObjects;
                DestroyImmediate(gob);
            }

            EditorGUI.PropertyField(
                new Rect(rect.x +buttonWidth +spaceWidth, rect.y, nameWidth, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("position"), GUIContent.none);

            EditorGUI.LabelField(
                new Rect(rect.x + buttonWidth + spaceWidth *2 + nameWidth, rect.y, labelWidth, EditorGUIUtility.singleLineHeight),
                "direction"
            );

            EditorGUI.PropertyField(
                new Rect(rect.x + buttonWidth + labelWidth + nameWidth + spaceWidth*3, rect.y, directionWidth, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("direction"), GUIContent.none);
        };


    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        nodeList.DoLayoutList();


        serializedObject.ApplyModifiedProperties();
    }
}
#endif