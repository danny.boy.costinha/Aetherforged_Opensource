﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/dissipate" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Dissipate ("Clipping texture", 2D) = "white" {}
		_Clipamount ("Clipping amount", range(0,1)) = 1
	}
	SubShader 
	{

	pass
	{
	CGPROGRAM

	#pragma vertex vertexFunction
	#pragma fragment fragmentFunction

	#include "UnityCG.cginc"

	//get info from the model
	struct appdata
	{
		float4 vertex : POSITION;
		float2 uv :TEXCOORD0;
		float3 normal : NORMAL;
	};

	struct v2f
	{
		float4 position : SV_POSITION;
		float2 uv : TEXCOORD0;
	};

	//bring properties
	float4 _Color;
	sampler2D _MainTex;
	sampler2D _Dissipate;
	float _Clipamount;

	//set out the vertices
	v2f vertexFunction(appdata IN)
	{
		v2f OUT;

		OUT.position = UnityObjectToClipPos(IN.vertex);
		OUT.uv = IN.uv;

		return OUT;
	}

	//color inside vertices
	fixed4 fragmentFunction(v2f IN) : SV_Target
	{
		float4 textureColor = tex2D( _MainTex, IN.uv);
		float4 cliptexture = tex2D(_Dissipate, IN.uv);
		clip((textureColor + cliptexture) - _Clipamount);
		return textureColor * _Color;
	}
	ENDCG
	}

	}
}
