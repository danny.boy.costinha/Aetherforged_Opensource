﻿Shader "Custom/Distortion Shader" {
Properties {
	_TintColor ("Tint Color", Color) = (0.5,0.5,0.5,0.5)
	_MainTex ("Particle Texture", 2D) = "white" {}
	_MaskTex ("Texture Mask", 2D) = "white" {}
	_InvFade ("Soft Particles Factor", Range(0.01,3.0)) = 1.0
	_AlphaMultiplier("Alpha Multiplier", Range(0.0,10.0)) = 1.0
	_DistortMultiplier("Distort Multiplier", Range(0.0,10.0)) = 1.0
}

Category {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" }
	Blend SrcAlpha OneMinusSrcAlpha
	ColorMask RGB
	Cull Off Lighting Off ZWrite Off

	SubShader {

		GrabPass{

			"_SideTex1"

		}

		Pass {
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile_particles
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			sampler2D _MaskTex;
			sampler2D _SideTex1;
			fixed4 _TintColor;
			float _AlphaMultiplier;
			float _DistortMultiplier;


			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float4 texcoord : TEXCOORD0;
				float2 texcoord1 : TEXCOORD1;
				float2 texcoord2 : TEXCOORD2;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				float4 texcoord : TEXCOORD0;
				float2 texcoord1 : TEXCOORD1;
				float2 texcoord2 : TEXCOORD2;
				UNITY_FOG_COORDS(1)
			};
			
			float4 _MainTex_ST;
			float4 _MaskTex_ST;
			float4 _SideTex1_ST;

			v2f vert (appdata_t v)
			{
				v2f o;

//				#ifdef SOFTPARTICLES_ON
//				o.projPos = ComputeScreenPos (o.vertex);
//				COMPUTE_EYEDEPTH(o.projPos.z);
//				#endif
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.texcoord = ComputeGrabScreenPos(o.vertex);
				o.texcoord1 = TRANSFORM_TEX(v.texcoord1,_MainTex);
				o.texcoord2 = TRANSFORM_TEX(v.texcoord2,_MaskTex);
				o.color = v.color;

				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			sampler2D_float _CameraDepthTexture;
			float _InvFade;

			fixed4 frag (v2f i) : SV_Target
			{
				#ifdef SOFTPARTICLES_ON
				//float sceneZ = LinearEyeDepth (SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos))); - Commented for preventing builds DD 24/10
				//float partZ = i.projPos.z; - Commented for preventing builds DD 24/10
				//float fade = saturate (_InvFade * (sceneZ-partZ)); - Commented for preventing builds DD 24/10
				//i.color.a *= fade; - Commented for preventing builds DD 24/10
				#endif

				half4 bump = tex2D(_MainTex, i.texcoord1);
				half2 distortion = UnpackNormal(bump);

				i.texcoord.xy += distortion * _DistortMultiplier;

				i.color.a*=_AlphaMultiplier;

				fixed4 col = i.color * tex2Dproj(_SideTex1, (i.texcoord)) * tex2D(_MaskTex, (i.texcoord2));
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG 
		}

	}	
}
}
