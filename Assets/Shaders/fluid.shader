﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/fluid" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_MaskTex ("Texture Mask", 2D) = "white" {}
		_Extrudeint ("Extrude amount", range(-5,5)) = 0
		_Breathingspeed ("Wave speed", range(0,100)) = 0
		diffusestrength ("Diffuse Strength", range(-5,5)) = 0
		normalamount ("Normal amount", range(-10,10)) = 0
	}
	SubShader 
	{

	pass
	{
	Tags { "Queue"="Transparent" "IgnoreProjector"="False" "RenderType"="Opaque" "PreviewType"="Plane" }
	Blend SrcAlpha OneMinusSrcAlpha
	ColorMask RGB
	Cull Off Lighting On ZWrite On

	CGPROGRAM

	#pragma vertex vertexFunction
	#pragma fragment fragmentFunction
	#pragma target 3.0 

	#include "UnityCG.cginc"
	#include "UnityLightingCommon.cginc"


	//get info from the model
	struct appdata
	{
		float4 vertex : POSITION;
		float2 uv :TEXCOORD0;
		float2 texcoord2 : TEXCOORD2;
		float3 normal : NORMAL;
	};

	struct v2f
	{
		float4 position : SV_POSITION;
		float2 uv : TEXCOORD0;
		float2 texcoord2 : TEXCOORD2;
		float3 normal : NORMAL;
	};

	//bring properties
	float4 _Color;
	sampler2D _MainTex;
	sampler2D _MaskTex;
	float _Extrudeint;
	float _Breathingspeed;
	float4 _MaskTex_ST;

	//set out the vertices
	v2f vertexFunction(appdata IN)
	{
		v2f OUT;

		half offsetvertices = sin(IN.vertex.x);
		half value= _Extrudeint*sin(_Time.y * (_Breathingspeed) + offsetvertices);
		IN.vertex.xyz += value;
		IN.texcoord2.x +=(_Time.y*(_Breathingspeed*_Extrudeint)/10);

		OUT.position = UnityObjectToClipPos(IN.vertex);
		OUT.texcoord2 = TRANSFORM_TEX(IN.texcoord2,_MaskTex);
	

		return OUT;
	}

	//color inside vertices
	fixed4 fragmentFunction(v2f IN) : SV_Target
	{
		half4 bump = tex2D(_MaskTex, IN.texcoord2/2);

		float4 textureColor = tex2D( _MainTex, IN.uv) * tex2D( _MainTex, IN.texcoord2);

		return textureColor * _Color;
	}

	ENDCG
	}

	pass{
	Tags { "Queue"="Transparent" "IgnoreProjector"="False" "RenderType"="Transparent" "PreviewType"="Plane" }
	Blend SrcColor One
	Cull Off Lighting On ZWrite On

	CGPROGRAM

	#pragma vertex vertexFunction
	#pragma fragment fragmentFunction
	#pragma target 3.0 

	#include "UnityLightingCommon.cginc"
	#include "UnityCG.cginc"

	//get info from the model
	struct appdata
	{
		float4 vertex : POSITION;
		float2 uv :TEXCOORD0;
		float2 texcoord2 : TEXCOORD2;
		float3 normal : NORMAL;
	};

	struct v2f
	{
		float4 position : SV_POSITION;
		float2 uv : TEXCOORD0;
		float2 texcoord2 : TEXCOORD2;
		float3 normal : NORMAL;
		fixed4 diff : COLOR0;
		half3 worldRefl : TEXCOORD3;
		float2 screenuv : screenuv;
		float depth : DEPTH;
	};

	//bring properties
	float4 _Color;
	sampler2D _MainTex;
	sampler2D _MaskTex;
	float _Extrudeint;
	float _Breathingspeed;
	float4 _MaskTex_ST;
	float diffusestrength;
	float normalamount;

	//set out the vertices
	v2f vertexFunction(appdata IN)
	{
		v2f OUT;

		half offsetvertices = sin(IN.vertex.x);
		half value= _Extrudeint*sin(_Time.y * (_Breathingspeed) + offsetvertices);
		IN.vertex.xyz += value*0.8;
		if(_Extrudeint < 0){
		IN.vertex.y+=-_Extrudeint/2;
		}
		else{
		IN.vertex.y+=_Extrudeint/2;
		}

		IN.texcoord2.x +=(_Time.y*(_Breathingspeed*_Extrudeint)/6);
		OUT.position = UnityObjectToClipPos(IN.vertex);
		OUT.texcoord2 = TRANSFORM_TEX(IN.texcoord2,_MaskTex);

		return OUT;
	}

	//color inside vertices
	fixed4 fragmentFunction(v2f IN) : SV_Target
	{
		float screenDepth = DecodeFloatRG(tex2D(_MainTex, IN.texcoord2).zw);
		float diff = screenDepth - IN.depth;
		float intersect = 0;

		if(diff > 0){
			intersect = 1 - smoothstep(0, _ProjectionParams.w, diff);
			}

		half4 bump = tex2D(_MaskTex, IN.texcoord2/2);
		half2 distortion = UnpackNormal(bump);
		IN.texcoord2.xy += distortion/normalamount;

		float4 textureColor = tex2D( _MainTex, IN.texcoord2);
		_Color.a = 0.75;
		return _Color* diffusestrength * textureColor;
	}
	ENDCG
	}



}
fallback "Diffuse"
}