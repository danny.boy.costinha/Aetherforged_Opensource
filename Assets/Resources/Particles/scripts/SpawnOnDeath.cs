﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnOnDeath : MonoBehaviour {
    // Note: this is basically the same idea as PlayOnDeath, but for arbitrary prefabs

    [SerializeField]
    GameObject whenIDieSpawnThisPrefab;


    void OnDestroy ()
    {
        Instantiate (whenIDieSpawnThisPrefab, transform.position, Quaternion.identity); 
    }
}
